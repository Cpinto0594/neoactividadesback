

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cors = require('cors');
var bodyParser = require('body-parser');

var MysqlBD = require('./app/persistence/Mysql/MysqlPoolProvider');

var tokenChecker = require('./app/tokenGenerator/tokenchecker');
var apiRouter = require('./app/apiRoutes/app/appRoutes');
var empresasRouter = require('./app/apiRoutes/empresas/empresas.routes');
var positionsRouter = require('./app/apiRoutes/positions/positions.routes');
var proyectosRouter = require('./app/apiRoutes/proyectos/proyectos.routes');
var usuariosRouter = require('./app/apiRoutes/usuarios/usuarios.routes');
var actividadesRouter = require('./app/apiRoutes/actividades/actividades.routes');
var accessRouter = require('./app/apiRoutes/access/access.routes');
var postRouter = require('./app/apiRoutes/social/posts/posts.routes');
var connectionsRouter = require('./app/apiRoutes/social/connections/connections.routes');
var chatsRouter = require('./app/apiRoutes/social/chat/chat.routes');


var fileUpload = require('express-fileupload');



var app = express();

app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'dist/activityControl')));
app.use('/', express.static(path.join(__dirname, 'dist/activityControl')));

//Change Request Size Limit
app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));
//FileUpload Middleware
app.use(fileUpload({
  limits: { fileSize: 50 * 1024 * 1024 },
}));

var origins = [
  "http://3.16.157.87",
  "https://neoactivities-c72c4.web.app",
  "https://neoactivities-c72c4.firebaseapp.com",
  "*",
  "http://ec2-3-16-157-87.us-east-2.compute.amazonaws.com",
  "ec2-3-16-157-87.us-east-2.compute.amazonaws.com",
  "http://localhost:4200"
]
//USE CORS
app.use(cors({
  origin: origins
}));


//BD Initialization
MysqlBD.initPool();


//Publish routes V1
app.use('/', apiRouter);
app.use('/api/v1/access', accessRouter);

// ############## ACTIVITY CONTROL ##################
app.use('/api/v1/empresas', tokenChecker.checkTokenMiddleWare, empresasRouter);
app.use('/api/v1/proyectos', tokenChecker.checkTokenMiddleWare, proyectosRouter);
app.use('/api/v1/usuarios', tokenChecker.checkTokenMiddleWare, usuariosRouter);
app.use('/api/v1/actividades', tokenChecker.checkTokenMiddleWare, actividadesRouter);
app.use('/api/v1/positions', tokenChecker.checkTokenMiddleWare, positionsRouter);



// ############## SOCIAL ##################
app.use('/api/v1/social/posts', tokenChecker.checkTokenMiddleWare, postRouter);
app.use('/api/v1/social/connections', tokenChecker.checkTokenMiddleWare, connectionsRouter);
app.use('/api/v1/social/chats', tokenChecker.checkTokenMiddleWare, chatsRouter);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log(err)
  // render the error page
  res.status(err.status || 500);
  res.sendStatus(err.status);
});

module.exports = app;
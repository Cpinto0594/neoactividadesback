var router = require('express').Router();
var UserServices = (require('../../apiServices/usuarios/usuarios.services'));
var SystemAppService = (require('../../apiServices/systemapp/systemapp.services'));
var UserAppConfigService = (require('../../apiServices/userappconfig/userappconfig'));
var UserExcludedViewsService = (require('../../apiServices/userexcludedviews/userexcludedviews'));
var UserRolesService = (require('../../apiServices/roles/userroles.services'));
var MysqlTransactionProvider = require('../../persistence/Mysql/MysqlTransactionProvider');
var UserPositionsService = require('../../apiServices/positions/positions.services');
var ResponseObject = require('../ResponseObject');
var tokenChecker = require('../../tokenGenerator/tokenchecker');
var appConfig = require('../../config/appConfig');
var tokenConfig = require('../../tokenGenerator/configuration');

router

    .post('/login', async (req, res, next) => {
        let usuariosService = new UserServices;
        let userAppConfigService = new UserAppConfigService;
        let userRolesService = new UserRolesService;
        let userExcludedViewsService = new UserExcludedViewsService;
        let Transaction = new MysqlTransactionProvider;
        let userPositionService = new UserPositionsService

        try {
            var data = req.body;
            var loginData = data;

            if (!data || !loginData.usuario) {
                res.json(new ResponseObject(req, false, 'No hay parámetros de entrada'))
                return;
            }

            //Begin
            await Transaction.beginTransaction();

            await usuariosService.repository.setCurrentTransaction(Transaction);
            await userAppConfigService.repository.setCurrentTransaction(Transaction);
            await userRolesService.repository.setCurrentTransaction(Transaction);
            await userExcludedViewsService.repository.setCurrentTransaction(Transaction);
            await userPositionService.repository.setCurrentTransaction(Transaction);

            let response = await usuariosService.login(loginData)

            if (response) {
                var data = response;
                if (data.success) {

                    let userToken = tokenChecker.createToken(req, {
                        user: data.usuario
                    }, tokenConfig.userTokenAlive);

                    let refreshToken = tokenChecker.createToken(req, {
                        user: data.usuario
                    }, tokenConfig.refreshTokenAlive);
                    //User Session Token
                    data.userToken = userToken;
                    data.RfTkn = refreshToken;


                    //User App Config
                    let userConfigData = await userAppConfigService.get([], { user_name: loginData.usuario });
                    data.userAppConfig = {};

                    if (userConfigData && userConfigData.length) {
                        userConfigData = userConfigData[0];
                        data.userAppConfig = {
                            menuStyle: userConfigData.app_menu_style,
                            appUpdateNotification: userConfigData.app_update_notification,
                            appNoActivitiesNotification: userConfigData.app_noactivities_notification,
                            configId: userConfigData.id
                        };
                    }

                    //Roles del Usuario
                    let userRoles = await userRolesService.findUserRoles(+data.userId);
                    data.userRoles = userRoles;


                    //Vistas Excluidas
                    let excludedViews = await userExcludedViewsService.excludedViews(+data.userId);
                    data.userExcludedViews = excludedViews;

                    if (data.userPositionId) {
                        let position = (await userPositionService.get({}, { id: data.userPositionId }))[0];
                        data.userPosition = position.description;
                    }

                }
                //Commit
                await Transaction.commitTransaction();

                res.json(new ResponseObject(req, data.success, data.message, data));
            } else {

                throw Error('No se pudo obtener respuesta del servidor');
            }



        } catch (e) {
            //Rollback
            console.log(e)
            await Transaction.rollbackTransaction();
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/check', async function (req, resp, next) {

        try {
            var result = await tokenChecker.checkToken(req);
            if (result && !result.success) {
                resp.status(403).json(result);
                return;
            }
            resp.json(result);

        } catch (e) {
            resp.json(new ResponseObject(req, false, e.message));
        }

    })
    .post('/check-app-token', async function (req, resp, next) {

        try {
            var result = await tokenChecker.chackTokenV2(req);
            if (result && !result.success) {
                resp.status(403).json(result);
                return;
            }
            resp.json(result);

        } catch (e) {
            resp.json(new ResponseObject(req, false, e.message));
        }

    })
    .get('/app-info', async function (req, resp, next) {
        let systemAppService = new SystemAppService;
        try {

            var requestKey = req.headers[appConfig.MOBILE_APP_APP_KEY_HEADER.toLowerCase()];
            var requestVersion = req.headers[appConfig.MOBILE_APP_APP_VERSION_HEADER.toLowerCase()];

            if (!requestKey) {
                throw Error('No se pudo validar Llave de acceso de tu app');
            }

            if (!requestVersion) {
                throw Error('No podemos validar la version de tu app.');
            }

            //Traemos informacion de la app
            let result = await systemAppService.get([], { app_id: requestKey });
            if (!result || !result[0]) {
                throw Error('App Cliente no registrada');
            }
            result = result[0];

            //Validamos la version de la App
            let version = result.app_version;
            let state = result.app_state;
            if (version !== requestVersion) {
                throw Error('La Version de tu app no corresponde a la minima version permitida.');
            }

            if (state !== 'A') {
                throw Error('La Aplicación no se encuentra activa, comuniquese con sistemas.');
            }

            resp.json(new ResponseObject(req, true, 'OK', result));

        } catch (e) {
            resp.status(403).json(new ResponseObject(req, false, e.message));
        }

    });



module.exports = router;
var router = require('express').Router();
var ActivityServices = (require('../../apiServices/actividades/actividades.services'))
var ResponseObject = require('../ResponseObject');


router
    .post('/export', function (req, res, next) {
        let actividadesService = new ActivityServices
        try {

            actividadesService.export(req.body).then(
                (data) => {
                    if (data) data = data[0];
                    res.json(new ResponseObject(req, true, 'OK', data))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .get('/find/:id', function (req, res, next) {
        let actividadesService = new ActivityServices
        try {
            actividadesService.get([], { id: req.params.id }).then(
                (data) => {
                    if (data) data = data[0];
                    res.json(new ResponseObject(req, true, 'OK', data))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .get('/getAllActive', function (req, res, next) {
        let actividadesService = new ActivityServices
        try {
            actividadesService.get([], { estado: 'A' }).then(
                (data) => {
                    res.json(new ResponseObject(req, true, 'OK', data))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/', function (req, res, next) {
        let actividadesService = new ActivityServices
        try {
            actividadesService.save(req.body)
                .then(
                    (data) => {
                        //Retornamos la actividad con el ID
                        var resut = {
                            ...req.body,
                            id: data.insertId
                        }
                        res.json(new ResponseObject(req, true, 'OK', resut))
                    },
                    (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
                );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/createAll', function (req, res, next) {
        let actividadesService = new ActivityServices
        try {
            actividadesService.createAll(req.body)
                .then(
                    (data) => {
                        res.json(new ResponseObject(req, true, 'OK', data))
                    },
                    (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
                );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .delete('/:id', function (req, res, next) {
        let actividadesService = new ActivityServices
        if (!req.params.id) {
            res.json(new ResponseObject(req, false, 'Parámetro ID no especificado'))
            return;
        }
        try {
            var data = { id: req.params.id };
            actividadesService.remove(data).then(
                (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .put('/:id', function (req, res, next) {


        if (!req.params.id) {
            res.json(new ResponseObject(req, false, 'Parámetro ID no especificado'))
            return;
        }
        let actividadesService = new ActivityServices
        try {
            var condition = { id: req.params.id };
            var data = req.body;

            actividadesService.update(data, condition).then(
                (respon) => { res.json(new ResponseObject(req, true, 'OK', data)) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/dashboard', async function (req, resp, next) {
        let actividadesService = new ActivityServices
        try {

            let data = req.body;
            if (!data.usuario || !data.periodicidad) {
                resp.json(new ResponseObject(req, false, 'Usuario o Periodicidad no fueron enviadas.'))
                return;
            }

            var response = await actividadesService.dashboard(data);

            resp.json(new ResponseObject(req, true, 'OK', response))



        } catch (error) {
            resp.json(new ResponseObject(req, false, error.message))
        }

    })
    .get('/autocomplete/:usuario', async function (req, resp, next) {
        let actividadesService = new ActivityServices
        try {

            let query = req.query.filtro;
            let usuario = req.params.usuario;
            if (!query) {
                resp.json(new ResponseObject(req, false, 'Debe enviar palabra para búsqueda.'));
                return;
            }

            var response = await actividadesService.autocomplete(query, usuario);
            resp.json(new ResponseObject(req, true, 'OK', response))



        } catch (error) {
            resp.json(new ResponseObject(req, false, error.message))
        }

    });



module.exports = router;
var router = require('express').Router();
var PositionsServices = (require('../../apiServices/positions/positions.services'))
var ResponseObject = require('../ResponseObject');


router
    .get('/', function (req, res, next) {
        let positionsServices = new PositionsServices
        try {
            positionsServices.get().then(
                (data) => { res.json(new ResponseObject(req, true, 'OK', data)) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .get('/find/:id', function (req, res, next) {
        let positionsServices = new PositionsServices
        try {
            positionsServices.get([], { id: req.params.id }).then(
                (data) => {
                    if (data) data = data[0];
                    res.json(new ResponseObject(req, true, 'OK', data))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .get('/getAllActive', function (req, res, next) {
        let positionsServices = new PositionsServices

        try {
            positionsServices.get([], { state: 'A' }).then(
                (data) => { res.json(new ResponseObject(req, true, 'OK', data)) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/', function (req, res, next) {
        let positionsServices = new PositionsServices
        try {
            positionsServices.save(req.body)
                .then(
                    (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                    (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
                );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .delete('/:id', function (req, res, next) {
        let positionsServices = new PositionsServices
        try {
            var data = { id: req.params.id };
            positionsServices.remove(data).then(
                (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .put('/:id', function (req, res, next) {

        let positionsServices = new PositionsServices

        try {
            var condition = { id: req.params.id };
            var data = req.body;

            positionsServices.update(data, condition).then(
                (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/search', function (req, res, next) {

        let positionsServices = new PositionsServices
        try {

            var data = req.body;
            var response = [];
            if (!data) {
                res.json(new ResponseObject(req, true, '', response));
            }
            var searchString = (data.search || '').toLowerCase();
            var query = 'select p.* from ' + positionsServices.collection + ' p where lower( p.code ) like lower( ? ) or lower( p.description ) like lower( ? )';
            var dataFilter = [searchString + '%', searchString + '%'];

            positionsServices.query(query, dataFilter).then(
                (succ) => { response = succ; res.json(new ResponseObject(req, true, '', response)) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) })

        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }

    });



module.exports = router;
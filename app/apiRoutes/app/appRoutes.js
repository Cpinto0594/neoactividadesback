var express = require('express');
var router = express.Router();
var tokenChecker = require('../../tokenGenerator/tokenchecker');


router.get('/', function (req, res, next) {
  res.json({ exito: true, message: 'Index of Backend' });
});

router.post('/createToken', function (req, res, next) {
  res.json({ exito: true, message: tokenChecker.createToken(req , req.body) });
});


module.exports = router;

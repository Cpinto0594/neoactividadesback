var router = require('express').Router();
var ResponseObject = require('../../ResponseObject');
const GroupsService = (require('../../../apiServices/socialconnections/socialgroups.services'));
const GroupsMembersService = (require('../../../apiServices/socialconnections/socialgroupsmembers.services'));
const UserServices = (require('../../../apiServices/usuarios/usuarios.services'));
const TransactionProvider = (require('../../../persistence/Mysql/MysqlTransactionProvider'));
const uuid = require('uuid/v4');

router
    //################   GET   ##################
    //GET USER CONNECTED PEOPLE
    .get('/:userUuid/people', async (req, res, next) => {

        let userServices = new UserServices;

        try {

            let userUUid = req.params.userUuid;

            let data = await userServices.query(
                `select uuid, nombre_completo, usuario, pic, email
                   from ${userServices.collection} where 
                   estado = ? and uuid != ? `,
                ['A', userUUid]);

            res.json(new ResponseObject(req, true, 'OK', data));

        } catch (error) {
            res.json(new ResponseObject(req, false, error.message))
        }
    })
    .get('/:userUuid/groups', async function (req, res, next) {

        let Transaction = new TransactionProvider
        let groupsService = new GroupsService
        let groupsMembersService = new GroupsMembersService

        try {
            let userId = req.params.userUuid;
            let params = req.query;
            let page = params.page;
            let max_rows = params.max_rows;

            await Transaction.beginTransaction();

            await groupsService.repository.setCurrentTransaction(Transaction);
            await groupsMembersService.repository.setCurrentTransaction(Transaction);

            let data = await groupsService.findUserGroups(userId,
                [page === 1 ? (page - 1) : ((page - 1) * max_rows), +params.max_rows]
            )

            for (let group of data) {
                let members = await groupsMembersService.findGroupMembersByUuid(group.uuid);
                group.members = members;
            }

            await Transaction.commitTransaction();
            res.json(new ResponseObject(req, true, 'OK', data))

        } catch (error) {
            await Transaction.rollbackTransaction();
            res.json(new ResponseObject(res, false, error.message))
        }

    })
    .get('/groups/:groupUuid/members', async function (req, res, next) {

        let Transaction = new TransactionProvider
        let groupsService = new GroupsService
        let groupsMembersService = new GroupsMembersService
        let userServices = new UserServices;


        try {
            let userId = req.params.userUuid;
            let groupUuid = req.params.groupUuid;
            let params = req.query;
            let page = params.page;
            let max_rows = params.max_rows;

            await Transaction.beginTransaction();

            await groupsMembersService.repository.setCurrentTransaction(Transaction);
            await groupsService.repository.setCurrentTransaction(Transaction);

            let group = (await groupsService.get([], { uuid: groupUuid, state: 'A' }))[0];
            if (!group) {
                throw Error(`Group ${groupUuid} does not exist`)
            }

            let data = await groupsService.query(
                `select us.uuid, us.nombre_completo, us.pic, us.email , mem.group_member_role
                   from ${userServices.collection} us
                   join ${groupsMembersService.collection} mem on mem.group_member_id = us.id
                  where mem.group_id = ? `,
                [group.id]
            );

            Transaction.commitTransaction();

            res.json(new ResponseObject(req, true, 'OK', data))

        } catch (error) {
            Transaction.rollbackTransaction();
            res.json(new ResponseObject(res, false, error.message))
        }

    })
    .post('/groups', async (req, res) => {

        let Transaction = new TransactionProvider
        let groupsService = new GroupsService
        let groupsMembersService = new GroupsMembersService
        let userServices = new UserServices;

        try {

            let data = req.body;
            if (!data) {
                throw Error('No se ha enviado información del grupo a registrar');
            }

            let userUUid = data.owner_uuid;

            //await Transaction.getConnectionInstance();
            await Transaction.beginTransaction();

            await groupsService.repository.setCurrentTransaction(Transaction);
            await groupsMembersService.repository.setCurrentTransaction(Transaction);
            await userServices.repository.setCurrentTransaction(Transaction)

            let userOwner = await userServices.findByUuid(userUUid);

            if (!userOwner) {
                throw Error(`No se encontró usuario ${userUUid}`);
            }

            let group_members = data.members;
            if (group_members) {
                group_members = JSON.parse(group_members);
            }
            let group_data = {
                group_description: data.group_description,
                group_privacy: data.group_privacy,
                group_created_at: new Date(),
                group_owner: userOwner.id,
                state: 'A',
                uuid: uuid()
            };

            let group_photo = (req.files || {}).photo;
            if (group_photo) {
                let response = await groupsService.saveGroupImagesAwsS3(group_data.uuid, group_photo);
                group_data.group_photo = response.image_path;
            }

            //Gruardamos Grupo
            let grouResponse = await groupsService.save(group_data);
            group_data.id = grouResponse.insertId;
            group_data.is_group_owner = 1;
            group_data.group_members = 1;

            //Registramos al creador del grupo como miembro administrador
            let memberData = {
                group_member_id: userOwner.id,
                group_id: grouResponse.insertId,
                group_member_role: 'ADMIN',
                state: 'A',
                group_member_join_date: new Date()

            };

            await groupsMembersService.save(memberData);

            if (group_members) {
                group_data.group_members = group_members.length + 1;

                for (let member of group_members) {
                    let userMember = await userServices.findByUuid(member.group_member_uuid);

                    let memberData = {
                        group_member_id: userMember.id,
                        group_id: grouResponse.insertId,
                        group_member_role: 'MEMBER',
                        state: 'A',
                        group_member_join_date: new Date()

                    };
                    await groupsMembersService.save(memberData);
                }
            }


            await Transaction.commitTransaction();

            res.json(new ResponseObject(res, true, 'OK', group_data))

        } catch (error) {
            console.log(error.message);
            await Transaction.rollbackTransaction();
            res.json(new ResponseObject(res, false, error.message))
        }
    })
    .put('/groups/:groupUUid', async (req, res) => {

        let Transaction = new TransactionProvider
        let groupsService = new GroupsService
        let groupsMembersService = new GroupsMembersService
        let userServices = new UserServices;


        try {

            let groupUuid = req.params.groupUUid;
            let data = req.body;
            if (!data) {
                throw Error('No se ha enviado información del grupo a registrar');
            }

            //await Transaction.getConnectionInstance();
            await Transaction.beginTransaction();

            await groupsService.repository.setCurrentTransaction(Transaction);
            await groupsMembersService.repository.setCurrentTransaction(Transaction);
            await userServices.repository.setCurrentTransaction(Transaction)

            let group_data = (await groupsService.get([], { uuid: groupUuid, state: 'A' }))[0];
            if (!group_data) {
                throw Error(`Group ${groupUuid} does not exist`);
            }

            let group_members = data.members;
            if (group_members) {
                group_members = JSON.parse(group_members);
                if (group_members.length) {
                    let allright = group_members.every(mem => !!mem.group_member_uuid)
                    if (!allright) {
                        throw Error(`Members without identifier have been sent, verify members data`);
                    }
                }
            }

            group_data.group_updated_at = new Date();
            group_data.group_description = data.group_description;
            group_data.group_privacy = data.group_privacy;

            let group_photo = (req.files || {}).photo;
            if (group_photo) {
                let response = await groupsService.saveGroupImagesAwsS3(group_data.uuid, group_photo);
                group_data.group_photo = response.image_path;
            }

            //Guardamos Grupo
            await groupsService.update(group_data, { id: group_data.id });
            group_data.is_group_member = 1;
            group_data.group_members = 1;


            if (group_members) {
                //Eliminamos los miembros anteriores para crear los nuevos miembros(Solo con rol MEMBER)
                await groupsMembersService.remove({ group_id: group_data.id, group_member_role: 'MEMBER' });

                group_data.group_members = group_members.length + 1;

                for (let member of group_members) {
                    let userMember = await userServices.findByUuid(member.group_member_uuid);


                    let memberData = {
                        group_member_id: userMember.id,
                        group_id: group_data.id,
                        group_member_role: 'MEMBER',
                        state: 'A',
                        group_member_join_date: new Date()

                    };
                    await groupsMembersService.save(memberData);
                }
            }

            await Transaction.commitTransaction();

            res.json(new ResponseObject(res, true, 'OK', group_data))

        } catch (error) {
            console.log(error.message);
            await Transaction.rollbackTransaction();
            res.json(new ResponseObject(res, false, error.message))
        }
    })
    .delete('/groups/:groupUUid', async (req, res) => {
        let groupsService = new GroupsService

        try {
            let groupUUid = req.params.groupUUid;
            await groupsService.repository.begin();

            let group = (await groupsService.get([], { uuid: groupUUid, state: 'A' }))[0];
            if (!group) {
                throw Error(`Group ${groupUUid} does not exist`);
            }
            group.state = 'I';
            await groupsService.update(group, { uuid: groupUUid, state: 'A' });


            await groupsService.repository.commit()

            res.json(new ResponseObject(req, true, 'OK'));

        } catch (error) {
            await groupsService.repository.rollback()
            res.json(new ResponseObject(req, false, error.message));
        }

    });




module.exports = router;
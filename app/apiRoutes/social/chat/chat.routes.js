var router = require('express').Router();
var ResponseObject = require('../../ResponseObject');
const chatsServices = new (require('../../../apiServices/socialconnections/socialchat.services'));


router
    //################   GET   ##################
    //GET USER CHATS
    .get('/:userUuid/chats', function (req, res, next) {
        res.json({
            ok: true
        })
    });
module.exports = router;
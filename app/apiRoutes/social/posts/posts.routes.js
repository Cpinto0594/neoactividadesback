var router = require('express').Router();
var SocialPostServices = (require('../../../apiServices/socialpost/socialpost.services'))
var SocialCommentsServices = (require('../../../apiServices/socialpost/socialcomments.services'))
var SocialLikesServices = (require('../../../apiServices/socialpost/sociallikes.services'))
var TransactionProvider = require('../../../persistence/Mysql/MysqlTransactionProvider');

const FeedSocialPostsFactory = require('../../../apiServices/socialpost/queryfactory/socialpost.feed.query');
const UserSocialPostsFactory = require('../../../apiServices/socialpost/queryfactory/socialpost.userposts.query');
const SocialPostsCommentsFactory = require('../../../apiServices/socialpost/queryfactory/socialpost.comments.query');

const uuid = require('uuid/v4')
var ResponseObject = require('../../ResponseObject');



router
    //################   GET   ##################
    //GET POST
    .get('/', function (req, res, next) {
        findFeedPosts(req, res)
    })
    .get('/:postUuid', function (req, res, next) {
        findFeedPosts(req, res)
    })
    .get('/user/:userUuid', function (req, res, next) {
        findUserPosts(req, res)
    })
    //################   GET   ##################


    //##############   DELETE   ##################
    //DELETE POST
    .delete('/:postUuid/', async (req, res) => {

        deletePosts(req, res, 'post')
    })
    //##############   DELETE   ##################


    //##############   POST   ################## 
    //SAVE POST
    .post('/', async (req, res, next) => {
        createPost(req, res, 'post');
    })
    //LIKE  POST
    .post('/:postUuid/like', async (req, res, next) => {
        likePost(req, res, 'post', true);
    })
    .delete('/:postUuid/like', async (req, res, next) => {
        likePost(req, res, 'post', false);
    })
    //##############   POST   ##################


    // ==============================//
    // ======= COMMENTS =============//

    //FIND POST COMMENTS
    .get('/:postUuid/comments', async (req, res) => {
        findFeedComments(req, res);
    })
    //CREATE POST COMMENT
    .post('/:postUuid/comment', async (req, res) => {
        createPost(req, res, 'comment');
    })
    //DELETE POST COMMENT
    .delete('/:postUuid/:commentUuid', async (req, res) => {
        deletePosts(req, res, 'comment')
    })
    //LIKE  COMMENT
    .post('/:postUuid/:commentUuid/like', async (req, res, next) => {
        likePost(req, res, 'comment', true);
    })
    .delete('/:postUuid/:commentUuid/like', async (req, res, next) => {
        likePost(req, res, 'comment', false);
    });



const findFeedPosts = async (req, res) => {

    let postFeed = new FeedSocialPostsFactory;

    try {

        postFeed.findPosts(req)
            .then(
                (data) => {
                    res.json(new ResponseObject(req, true, 'OK', data))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
    } catch (e) {
        res.json(new ResponseObject(req, false, e.message))
    }
}

const findFeedComments = async (req, res) => {

    let postComments = new SocialPostsCommentsFactory;

    try {

        postComments.findComments(req)
            .then(
                (data) => {
                    res.json(new ResponseObject(req, true, 'OK', data))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
    } catch (e) {
        res.json(new ResponseObject(req, false, e.message))
    }
}

const findUserPosts = async (req, res) => {

    let userPosts = new UserSocialPostsFactory;

    try {

        userPosts.findPosts(req)
            .then(
                (data) => {
                    res.json(new ResponseObject(req, true, 'OK', data))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
    } catch (e) {
        res.json(new ResponseObject(req, false, e.message))
    }
}

const createPost = async (req, res, type) => {

    let postsService = new SocialPostServices;
    let Transaction = new TransactionProvider;

    try {


        await Transaction.beginTransaction();
        await postsService.repository.setCurrentTransaction(Transaction);

        let data = req.body;
        let postUuid = req.params.postUuid; //EN caso de ser un comentario, este "postUUid" puede ser el Uuid de un POST O COMENTARIO
        if (!data) {
            res.json(new ResponseObject(req, false, 'No se enviaron datos'))
            return;
        }

        //Array con la información de las imagenes y los adjuntos
        let post_images = data['images'] ? JSON.parse(data['images']) : [];
        let post_attachments = data['attachments'] ? JSON.parse(data['attachments']) : [];
        let images_buffer, attachments_buffer;

        //Eliminamos los atributos innecesarios para el post
        delete data["attachments"];
        delete data["images"];

        //Guardamos el post/comentario
        let post = Object.assign({}, data);
        post.uuid = uuid();
        post.post_type = type;
        post.post_created_at = new Date();
        post.post_published_at = post.post_created_at;
        //Asignamos el id del post/comentario del padre
        if (postUuid) {
            post.post_parent_uuid = postUuid;
        }

        let savedPost = await postsService.save(post);

        if (req.files) {

            //Array con la información binaria de los archivos
            images_buffer = req.files.images_data || [];
            images_buffer = images_buffer instanceof Array ? images_buffer : [images_buffer];
            attachments_buffer = req.files.attachs_data || [];
            attachments_buffer = attachments_buffer instanceof Array ? attachments_buffer : [attachments_buffer];

        }

        if (images_buffer && images_buffer.length) {

            try {

                for (let image of images_buffer) {
                    let image_post = post_images.find(img => String(img.id) === image.name);
                    delete image_post["id"];

                    let imageS3 = type === 'post' ?
                        await postsService.savePostImagesAwsS3(post.uuid, image) :
                        await postsService.savePostCommentImagesAwsS3(postUuid, post.uuid, image);

                    image_post.post_id = savedPost.insertId;
                    image_post.image_url = imageS3.image_path;

                    let ext = image_post.image_name.substring(image_post.image_name.lastIndexOf('.') + 1, image_post.image_name.length)
                    image_post.image_store_name = `${image.name}.${ext}`;


                    let imageSaved = await postsService.savePostImage(image_post);
                    image_post.id = imageSaved.insertId;

                }
            } catch (error) {
                console.log(error)
                throw error;
            }

        }

        if (attachments_buffer && attachments_buffer.length) {
            try {

                for (let attachment of attachments_buffer) {
                    let att_post = post_attachments.find(att => String(att.id) === attachment.name);
                    delete att_post["id"];

                    let attS3 = type === 'post' ?
                        await postsService.savePostAttachmentsAwsS3(post.uuid, attachment) :
                        await postsService.savePostCommentAttachmentsAwsS3(postUuid, post.uuid, attachment);

                    att_post.post_id = savedPost.insertId;
                    att_post.attachment_url = attS3.attachment_path;

                    let ext = att_post.attachment_name.substring(att_post.attachment_name.lastIndexOf('.') + 1, att_post.attachment_name.length)
                    att_post.attachment_store_name = `${attachment.name}.${ext}`;

                    let attachmentSaved = await postsService.savePostAttachment(att_post);
                    att_post.id = attachmentSaved.insertId;

                }
            } catch (error) {
                console.log(error)
                throw error;
            }
        }

        post.images = post_images;
        post.attachments = post_attachments;
        post.author = await postsService.findAuthorById(post.post_user_owner_id);
        post.author = post.author[0];
        post.post_comments = 0;
        post.post_likes = 0;
        post.user_liked = false;

        await Transaction.commitTransaction();

        res.json(new ResponseObject(req, true, 'OK', post))
    } catch (e) {
        console.log(e);
        await Transaction.rollbackTransaction();
        res.json(new ResponseObject(req, false, e.message))
    }
}

const deletePosts = async (req, res, type) => {

    let postCommentsService = new SocialCommentsServices;

    try {

        let postUuid = req.params.postUuid;
        let commentUuid = req.params.commentUuid;


        let condition = {
            uuid: postUuid,
            post_type: type,
            state: 'A'
        }

        if (commentUuid) {
            condition.post_parent_uuid = postUuid;
            condition.uuid = commentUuid;
        }

        await postCommentsService.update({ state: 'I' }, condition);

        res.json(new ResponseObject(req, true, 'OK'))
    } catch (e) {
        res.json(new ResponseObject(req, false, e.message))
    }
}

const likePost = async (req, res, type, possitive) => {

    let Transaction = new TransactionProvider;
    let postLikesService = new SocialLikesServices;
    let postsService = new SocialPostServices;

    try {


        await Transaction.beginTransaction();

        await postLikesService.repository.setCurrentTransaction(Transaction);
        await postsService.repository.setCurrentTransaction(Transaction);


        let post_parent = req.params.postUuid;
        let post_child = req.params.commentUuid;

        let current_post = post_child || post_parent;

        let user = req.query.user;

        if (!user) {
            res.json(new ResponseObject(req, false, 'No se envió usuario.'))
            return;
        }

        let post = await postsService.get(['id'], { uuid: current_post, state: 'A', post_type: type })
        post = post[0];


        if (!post) {
            res.json(new ResponseObject(req, false, `No existe recurso ${current_post}`))
            return;
        }

        let userLike = await postsService.findUserByUserUuid(user);
        userLike = userLike[0];

        if (!userLike) {
            res.json(new ResponseObject(req, false, `No hay infomación del usuario que reacciona`))
            return;
        }
        if (possitive) {
            //Nuevo like

            let liked = (await postLikesService.get([], { post_id: post.id, like_user_id: userLike.id }))[0]
            if (!liked) {
                let reaction_data = {
                    post_id: post.id,
                    like_user_id: userLike.id,
                    like_date: new Date(),
                    state: 'A',
                    like_type: type
                }
                await postLikesService.save(reaction_data);
            }
        } else {
            //Eliminamos los likes
            await postLikesService.remove({ post_id: post.id, like_user_id: userLike.id })
        }

        let likesCount = await postLikesService.countPostLikes(post.id, type)

        await Transaction.commitTransaction();

        res.json(new ResponseObject(req, true, 'OK', {
            likes_count: (likesCount[0] || { count: 0 }).count
        }))
    } catch (e) {
        console.log(e)
        await Transaction.rollbackTransaction();
        res.json(new ResponseObject(req, false, e.message))
    }
}

module.exports = router;
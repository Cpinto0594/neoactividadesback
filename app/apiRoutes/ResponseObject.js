module.exports = class ResponseObject {
    constructor(req, success, message, data) {
        this.success = success;
        this.message = message;

        this.data = data;
        if (req) {
            //Agregamos info del request al response
            this.requestInfo = {
                protocol: req.protocol,
                host: req.get('host'),
                path: req.originalUrl,
                method: req.method
            }
            //Agregamos el nuevo token al response
            if (req.tokenRefresh) {
                this.usrTokenRevalidation = {
                    newToken: req.tokenRefresh.usrNewToken,
                }
            }
        }
    }
}
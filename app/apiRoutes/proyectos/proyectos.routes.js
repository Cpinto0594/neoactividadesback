var router = require('express').Router();
var ProyectsService = (require('../../apiServices/proyectos/proyectos.services'))
var ResponseObject = require('../ResponseObject');


router
    .get('/', function (req, res, next) {

        let proyectosService = new ProyectsService
        try {
            proyectosService.get().then(
                (data) => { res.json(new ResponseObject(req, true, 'OK', data)) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .get('/find/:id', function (req, res, next) {

        let proyectosService = new ProyectsService

        try {
            proyectosService.get([], { id: req.params.id }).then(
                (data) => {
                    if (data) data = data[0];
                    res.json(new ResponseObject(req, true, 'OK', data))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .get('/getAllActive', function (req, res, next) {
        let proyectosService = new ProyectsService
        try {
            proyectosService.get([], { estado: 'A' }).then(
                (data) => { res.json(new ResponseObject(req, true, 'OK', data)) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/', function (req, res, next) {
        let proyectosService = new ProyectsService
        try {
            proyectosService.save(req.body)
                .then(
                    (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                    (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
                );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .delete('/:id', function (req, res, next) {

        if (!req.params.id) {
            res.json(new ResponseObject(req, false, 'Parámetro ID no especificado'))
            return;
        }
        let proyectosService = new ProyectsService
        try {
            var data = { id: req.params.id };
            proyectosService.remove(data).then(
                (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .put('/:id', function (req, res, next) {


        if (!req.params.id) {
            res.json(new ResponseObject(req, false, 'Parámetro ID no especificado'))
            return;
        }
        let proyectosService = new ProyectsService
        try {
            var condition = { id: req.params.id };
            var data = req.body;

            proyectosService.update(data, condition).then(
                (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/search', function (req, res, next) {
        let proyectosService = new ProyectsService
        try {

            var data = req.body;
            var response = [];
            if (!data) {
                res.json(new ResponseObject(req, true, '', response));
            }
            var searchString = (data.search || '').toLowerCase();
            var query = 'select p.* from ' + proyectosService.collection + ' p where lower( p.codigo ) like lower( ? ) or lower( p.descripcion ) like lower( ? )';
            var dataFilter = [searchString + '%', searchString + '%'];

            proyectosService.query(query, dataFilter).then(
                (succ) => { response = succ; res.json(new ResponseObject(req, true, '', response)) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) })

        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }

    });



module.exports = router;
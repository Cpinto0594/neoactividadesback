var router = require('express').Router();
var CompanyServices = (require('../../apiServices/empresas/empresas.services'))
var ResponseObject = require('../ResponseObject');


router
    .get('/', function (req, res, next) {
        let empresasService = new CompanyServices
        try {
            empresasService.get().then(
                (data) => { res.json(new ResponseObject(req, true, 'OK', data)) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .get('/find/:id', function (req, res, next) {
        let empresasService = new CompanyServices
        try {
            empresasService.get([], { id: req.params.id }).then(
                (data) => {
                    if (data) data = data[0];
                    res.json(new ResponseObject(req, true, 'OK', data))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .get('/getAllActive', function (req, res, next) {
        let empresasService = new CompanyServices
        try {
            empresasService.get([], { estado: 'A' }).then(
                (data) => { res.json(new ResponseObject(req, true, 'OK', data)) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/', function (req, res, next) {
        let empresasService = new CompanyServices
        try {
            empresasService.save(req.body)
                .then(
                    (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                    (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
                );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .delete('/:id', function (req, res, next) {
        if (!req.params.id) {
            res.json(new ResponseObject(req, false, 'Parámetro ID no especificado'))
            return;
        }
        let empresasService = new CompanyServices

        try {
            var data = { id: req.params.id };
            empresasService.remove(data).then(
                (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .put('/:id', function (req, res, next) {


        if (!req.params.id) {
            res.json(new ResponseObject(req, false, 'Parámetro ID no especificado'))
            return;
        }

        let empresasService = new CompanyServices

        try {
            var condition = { id: req.params.id };
            var data = req.body;

            empresasService.update(data, condition).then(
                (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/search', function (req, res, next) {
        let empresasService = new CompanyServices
        try {

            var data = req.body;
            var response = [];
            if (!data) {
                res.json(new ResponseObject(req, true, '', response));
            }
            var searchString = (data.search || '').toLowerCase();
            var query = 'select p.* from ' + empresasService.collection + ' p where lower( p.codigo ) like lower( ? ) or lower( p.descripcion ) like lower( ? )';
            var dataFilter = [searchString + '%', searchString + '%'];

            empresasService.query(query, dataFilter).then(
                (succ) => { response = succ; res.json(new ResponseObject(req, true, '', response)) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) })

        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }

    });



module.exports = router;
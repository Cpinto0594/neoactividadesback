var router = require('express').Router();
var UsuariosService = (require('../../apiServices/usuarios/usuarios.services'));
var UserDevicesService = (require('../../apiServices/devices/devices.services'));
var ResponseObject = require('../ResponseObject');


const changeUserData = (user) => {
    user.clave = undefined;
    //user.pic = undefined;
}


router
    .get('/', function (req, res, next) {

        let usuariosService = new UsuariosService;

        try {
            usuariosService.get().then(
                (data) => {
                    var _data = data;
                    if (_data && _data.length) {
                        _data.forEach(element => {
                            changeUserData(element);
                        });
                    }
                    res.json(new ResponseObject(req, true, 'OK', _data))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .get('/find/:id', function (req, res, next) {
        let usuariosService = new UsuariosService;
        try {
            usuariosService.conAsociaciones(req.params.id).then(
                (data) => {
                    var _data = data.usuario;
                    if (_data && _data.length) {
                        _data.forEach(element => {
                            changeUserData(element);
                        });
                    }
                    data.usuario = _data[0] || {};
                    res.json(new ResponseObject(req, true, 'OK', data))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .get('/getAllActive', function (req, res, next) {
        let usuariosService = new UsuariosService;
        try {
            usuariosService.get([], { estado: 'A' }).then(
                (data) => {
                    var _data = data;
                    if (_data && _data.length) {
                        _data.forEach(element => {
                            changeUserData(element);
                        });
                    }
                    res.json(new ResponseObject(req, true, 'OK', _data))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/', function (req, res, next) {
        let usuariosService = new UsuariosService;
        try {
            usuariosService.save(req.body)
                .then(
                    (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                    (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
                );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .delete('/:id', function (req, res, next) {

        if (!req.params.id) {
            res.json(new ResponseObject(req, false, 'Parámetro ID no especificado'))
            return;
        }
        let usuariosService = new UsuariosService;

        try {
            var data = { id: req.params.id };
            usuariosService.remove(data).then(
                (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .put('/:id', function (req, res, next) {


        if (!req.params.id) {
            res.json(new ResponseObject(req, false, 'Parámetro ID no especificado'))
            return;
        }
        let usuariosService = new UsuariosService;

        try {
            var condition = { id: req.params.id };
            var data = req.body;

            usuariosService.update(data, condition).then(
                (data) => { res.json(new ResponseObject(req, true, 'OK')) },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) },
            );
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/createAndAsocciate', async function (req, res, next) {
        let usuariosService = new UsuariosService;

        try {

            var data = req.body;
            var response = await usuariosService.crearYAsociar(data);

            res.json(new ResponseObject(req, true, 'OK'));
        } catch (e) {
            e = e instanceof Promise ? (await e) : e;
            console.log(e)
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .put('/desAsociarEmpresas/:id', async function (req, res, next) {
        let usuariosService = new UsuariosService;

        try {
            var data = { empresa: req.query.empresa, usuario: req.params.id };
            await usuariosService.desAsociarEmpresa(data.usuario, data.empresa);
            res.json(new ResponseObject(req, true, 'OK'));
        } catch (e) {
            console.log('Error ', e)
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .put('/desAsociarProyectos/:id', async function (req, res, next) {
        let usuariosService = new UsuariosService;

        try {

            var data = { proyecto: req.query.proyecto, usuario: req.params.id };
            var response = await usuariosService.desAsociarProyecto(data.usuario, data.proyecto);
            res.json(new ResponseObject(req, true, 'OK'));
        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }
    })
    .post('/saveDeviceInfo/:usuario', async function (req, res, next) {

        let userDevicesServices = new UserDevicesService;
        try {

            await userDevicesServices.repository.begin()

            var data = req.body;
            var usuario = req.params.usuario;
            if (!usuario) {
                res.json(new ResponseObject(req, false, 'Debe proveer un usuario para actualizar información.'));
            }

            if (!data || !data.device_id) {
                res.json(new ResponseObject(req, false, 'Debe proveer información del dispositivo.'));
            }
            let device = await userDevicesServices.get([], { usuario: usuario });
            device = device[0];

            if (!device) {
                data.usuario = usuario;
                await userDevicesServices.save(data);
            } else {
                data.id = device.id;
                await userDevicesServices.update(data, { id: device.id });
            }

            await userDevicesServices.repository.commit()
            res.json(new ResponseObject(req, true, 'Registro Exitoso'));

        } catch (e) {
            await userDevicesServices.repository.rollback()
            res.json(new ResponseObject(req, false, e.message))
        }

    })
    .post('/search', function (req, res, next) {
        let usuariosService = new UsuariosService;
        try {

            var data = req.body;
            var response = [];
            if (!data) {
                res.json(new ResponseObject(req, true, '', response));
            }
            var searchString = (data.search || '').toLowerCase();
            var query = 'select p.* from ' + usuariosService.collection + ' p where lower( p.usuario ) like lower( ? ) '
                + ' or lower( p.nombre_completo ) like lower( ? )'
                + ' or lower( p.identificacion ) like lower( ? )';
            var dataFilter = [searchString + '%', searchString + '%', searchString + '%'];

            usuariosService.query(query, dataFilter).then(
                (succ) => {
                    response = succ;
                    response.forEach(us => {
                        us = changeUserData(us)
                    })
                    res.json(new ResponseObject(req, true, '', response))
                },
                (fail) => { res.json(new ResponseObject(req, false, fail.message)) })

        } catch (e) {
            res.json(new ResponseObject(req, false, e.message))
        }

    })
    .post('/uploadProfileImage/:usuario', async (req, res, next) => {
        let usuariosService = new UsuariosService;
        try {

            if (!req.files || !req.files.imageProfile) {
                res.json(new ResponseObject(req, false, 'No se envió imagen.'))
                return;
            }

            let usuario = req.params.usuario;
            let image = req.files.imageProfile;

            await usuariosService.repository.begin();
            let user = await usuariosService.get([], { usuario: usuario });
            user = user[0];

            if (!user) {
                res.json(new ResponseObject(req, false, `No existe usuario ${usuario}.`))
                return;
            }

            let respuesta = await usuariosService.saveImage(usuario, image);
            user.pic = respuesta.image_path;

            await usuariosService.update(user, { id: user.id });

            await usuariosService.repository.commit();

            res.json(new ResponseObject(req, true, 'OK', respuesta))

        } catch (e) {
            console.log(e);
            await usuariosService.repository.rollback();
            res.json(new ResponseObject(req, false, e.message))
        }

    });



module.exports = router;
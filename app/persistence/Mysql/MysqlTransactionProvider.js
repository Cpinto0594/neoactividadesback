const poolProvider = require('./MysqlPoolProvider');

class MySqlTransactionProvider {

    constructor() {
        this.connection = null;
        this.resourcesAttached = [];
    }

    getConnectionInstance() {
        return new Promise(async (succ, fail) => {
            try {
                //console.log('Transaction instance, hay conexion viva?', this.connectionAlive(this.connection))
                let connection = this.connection;
                if (!this.connectionAlive(connection)) {
                    connection = await poolProvider.openNewConnection();
                }
                this.connection = connection;
                succ(this.connection);

            } catch (error) {
                fail(error);
            }
        })
    }


    connectionAlive(connection) {
        return !!connection &&
            (connection.state !== 'disconnected') &&
            (connection._pool._freeConnections.indexOf(connection) === -1);
    }

    beginTransaction() {
        return new Promise(async (succ, fail) => {
            try {
                //Si no se ha creado la conexion o ya fue cerrada(Released)
                if (!this.connection || !this.connectionAlive(this.connection))
                    //Creamos una nueva conexion
                    await this.getConnectionInstance();


                if (this.connectionAlive(this.connection)) {
                    this.connection.beginTransaction((err) => {
                        if (err) fail(err);
                        else {
                            succ(this.connection)
                        }
                    })

                    return;
                }

                throw Error('Connection Closed');
            } catch (error) {
                fail(error);
            }
        })
    }

    commitTransaction() {
        return new Promise(async (succ, fail) => {
            try {
                if (this.connectionAlive(this.connection)) {
                    this.connection.commit(async (err) => {
                        if (err) fail(err);
                        else {
                            await this.handleConnectionEnd(this.connection)
                            succ()
                        }
                    })

                    return;
                }

                throw Error('Connection Closed');
            } catch (error) {
                fail(error);
            }
        })
    }

    rollbackTransaction() {
        return new Promise(async (succ, fail) => {
            try {
                if (this.connectionAlive(this.connection)) {
                    this.connection.rollback(async (err) => {
                        if (err) fail(err);
                        else {
                            await this.handleConnectionEnd()
                            succ()
                        }
                    })

                    return;
                }

                throw Error('Connection Closed');
            } catch (error) {
                fail(error);
            }
        })
    }

    injectServiceTransaction(service) {
        if (service) {
            service.currentTransaction = this.connection;
            this.resourcesAttached.push(service);
        }
    }

    async handleConnectionEnd() {
        if (this.connectionAlive(this.connection)) {
            await this.connection.release();
        }
    }
}

module.exports = MySqlTransactionProvider;

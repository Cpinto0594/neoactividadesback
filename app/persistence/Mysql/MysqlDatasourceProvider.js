const poolProvider = require('./MysqlPoolProvider');

class MySqlDatasourceProvider {

    constructor() {
        this.pool = poolProvider;
    }

    getConnection() {
        return new Promise(async (succ, fail) => {
            try {

                var connection = await this.pool.openNewConnection()
                succ(connection)
            } catch (error) {
                fail(error)
            }
        });
    }

    
}

module.exports = MySqlDatasourceProvider;

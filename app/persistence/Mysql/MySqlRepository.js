
var MySqlQueryFactory = require('./MySqlQueryFactory');
const MysqlTransactionProvider = require('./MysqlTransactionProvider');

class MySqlRepository extends MySqlQueryFactory {
    constructor(target) {
        super(target);
    }

    initRepository() {
        this.isLoaded = true;
        this.selfTransaction = true;
        this.transaction = new MysqlTransactionProvider;
    }

    setCurrentTransaction(Transaction) {
        Transaction.injectServiceTransaction(this);
        this.transaction = Transaction;
        this.selfTransaction = false;
        this.isLoaded = true;

    }

    connectionAlive(connection) {
        return !!connection &&
            (connection.state !== 'disconnected') &&
            (connection._pool._freeConnections.indexOf(connection) === -1);
    }

    isInSelfTransaction = () => {
        return this.selfTransaction && !!this.transaction
    }

    isInExternalTransaction = () => {
        return !this.selfTransaction && !!this.transaction
    }

    getCurrentTransaction(){
        return this.transaction;
    }

    requestConnection() {
        let $this = this;
        return new Promise(async (succ, fail) => {
            try {
                if (!this.isLoaded && !this.transaction) {
                    this.initRepository();
                }

                let connection = await $this.transaction.getConnectionInstance();
                succ(connection);

            } catch (e) {
                fail(e);
            }
        })
    }



    async handleConnectionEnd() {
        if (this.selfTransaction) {
            await this.transaction.handleConnectionEnd();
        }
    }

    get(columns, conditions) {
        var query = super.getQuery(columns, conditions);
        var dataCondition = super.saveDataObject(conditions);
        return new Promise(async (succ, fail) => {
            try {
                var conn = await this.requestConnection();

                conn.query(query, dataCondition, async(err, result) => {
                    if (err) fail(err);
                    else {
                        await this.handleConnectionEnd(conn);
                        succ(result);
                    }
                });

            } catch (e) {
                fail(e);
            }
        })

    }

    save(object) {
        var query = super.saveQuery(object);
        var data = super.saveDataObject(object);
        return new Promise((succ, fail) => {
            try {
                this.requestConnection().
                    then(async (connection) => {
                        connection.query(query, data, async (err, result) => {
                            if (err) fail(err);
                            else {
                                await this.handleConnectionEnd(connection);
                                succ(result);
                            }
                        });
                    });
            } catch (e) {
                fail(e);
            }
        });
    }

    delete(bind) {
        var query = super.deleteQuery(bind);
        var data = super.saveDataObject(bind);
        return new Promise((succ, fail) => {
            try {
                this.requestConnection().
                    then(async (connection) => {
                        connection.query(query, data, async (err, result) => {
                            if (err) fail(err);
                            else {
                               await this.handleConnectionEnd(connection);
                                succ(result);
                            }
                        });
                    });
            } catch (e) {
                fail(e);
            }
        });
    }

    update(bind, conditions) {
        var isArrayData = bind instanceof Array;

        if (isArrayData) throw new Error('No array data object espected');

        var query = super.updateQuery(bind, conditions);
        var data = super.saveDataObject(bind);
        var conditionData = super.updateDataCondition(conditions);
        data = data.concat(conditionData);
        return new Promise((succ, fail) => {
            try {
                this.requestConnection().
                    then(async (connection) => {
                        connection.query(query, data, async (err, result) => {
                            if (err) fail(err);
                            else {
                                await this.handleConnectionEnd(connection);
                                succ(result);
                            }
                        });
                    });
            } catch (e) {
                fail(e);
            }
        });
    }

    query(sqlQuery, data) {
        return new Promise((succ, fail) => {
            try {
                this.requestConnection().
                    then(async (connection) => {
                        connection.query(sqlQuery, data, async (err, result) => {
                            if (err) fail(err);
                            else {
                                await this.handleConnectionEnd(connection);
                                succ(result);
                            }
                        });
                    });
            } catch (e) {
                fail(e);
            }
        });
    }

    begin() {
        return new Promise(async (succ, fail) => {

            try {
                if (!this.isInSelfTransaction())
                    this.requestConnection()
                        .then((conn) => {
                            conn.beginTransaction((err) => {
                                if (err) fail(err);
                                else {
                                    ///Iniciamos la nueva transaccion
                                    succ(conn);
                                }
                            })
                        });
                else {
                    let connection = await this.transaction.getConnectionInstance();
                    succ(connection);
                }
            } catch (e) {
                fail(e);
            }

        });
    }

    commit() {
        return new Promise((succ, fail) => {

            try {
                if (this.isInSelfTransaction()) {

                    this.requestConnection()
                        .then(async (conn) => {
                            conn.commit(async (err) => {
                                if (err) fail(err);
                                else {
                                    await this.handleConnectionEnd()
                                    succ();
                                }
                            })
                        })
                } else {
                    succ();
                }

            } catch (e) {
                fail(e);
            }

        });
    }

    rollback() {
        return new Promise((succ, fail) => {

            try {
                if (this.isInSelfTransaction()) {

                    this.requestConnection()
                        .then(async (conn) => {
                            conn.rollback(async (err) => {
                                if (err) fail(err);
                                else {
                                    await this.handleConnectionEnd(conn);
                                    succ();
                                }
                            })
                        });
                } else {
                    succ();
                }

            } catch (e) {
                fail(e);
            }

        });
    }

}


module.exports = MySqlRepository;


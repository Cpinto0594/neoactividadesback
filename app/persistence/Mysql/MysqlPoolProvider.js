var mysql = require('mysql');
var config = require('../../config/appConfig');

class MySqlPoolProvider {
    static openNewConnection = () => {
        return new Promise((succ, fail) => {
            try {
                let pool = MySqlPoolProvider.prototype.__$$pool;
                if (!pool) throw Error('No Connection Pool created');
                pool.getConnection((err, conn) => {
                    if (err) throw err;
                    succ(conn);
                });

            } catch (error) {
                fail(error)
            }
        })
    }
    static initPool() {
        console.log('Initializing pool ' , config)
        MySqlPoolProvider.prototype.__$$pool =
            mysql.createPool({
                connectionLimit: 100,
                //queueLimit: 100,
                host: config.AWS_RDS_DATABASE_URL,
                user: config.AWS_RDS_DATABASE_USER,
                password: config.AWS_RDS_DATABASE_PASS,
                database: config.AWS_RDS_DATABASE_DATABASE,
                typeCast: function castField(field, useDefaultTypeCasting) {
                    // We only want to cast bit fields that have a single-bit in them. If the field
                    // has more than one bit, then we cannot assume it is supposed to be a Boolean.
                    if ((field.type === "BIT") && (field.length === 1)) {
                        var bytes = field.buffer();
                        // A Buffer in Node represents a collection of 8-bit unsigned integers.
                        // Therefore, our single "bit field" comes back as the bits '0000 0001',
                        // which is equivalent to the number 1.
                        return (bytes[0] === 1);
                    }
                    return (useDefaultTypeCasting());
                }
            });
    }
    static getPool() {
        return MySqlPoolProvider.prototype.__$$pool;
    }
}

module.exports = MySqlPoolProvider;

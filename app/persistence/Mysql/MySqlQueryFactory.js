class MySqlQueryFactory {


    constructor(target) {
        this.target = target;
        this.KEYS_NO_CONDITION = [
            'ORDER', 'LIMIT', 'GROUP', 'HAVING'
        ]

        this.OBJECT_NOT_CONDITION =
            {
                'ORDER': ' order by ',
                'LIMIT': ' limit ',
                'GROUP': ' group ',
                'HAVING': ' having '
            }

    }

    ////// SAVE /////////
    saveQuery(binds) {
        var query = this.getQueryTnsert()
            .replace('%columns%', this.getColumnsInsert(binds))
            .replace('%values%', this.getValuesInsert(binds));
        return query;
    }

    saveDataObject(data) {
        if (!data) return [];
        var _data = data instanceof Array ? data : [data];
        var dataReturn = _data.map(obj => {
            return Object.keys(obj).map(keys => obj[keys]);
        });

        if (data instanceof Array) {
            return [dataReturn];
        }
        return dataReturn[0];
    }

    getColumnsInsert(object) {
        if (!object) return '';

        var _object = object instanceof Array ? object[0] : object;
        var columns = Object.keys(_object);
        return columns.join(', ');

    }

    getValuesInsert(object) {
        if (!object) {
            return '';
        }
        var isArray = object instanceof Array;
        if (isArray) {
            return ' ?';
        } else {
            return '(%%)'.replace('%%', Object.keys(object).map(key => '?').join(', '));
        }
    }
    getQueryTnsert(target) {
        var _target = target || this.target;
        return 'INSERT INTO  ' + _target + '(%columns%)  VALUES%values% ';
    }

    ////// SAVE /////////

    ////// UPDATE /////////
    updateQuery(object, conditions) {
        var query = this.getQueryUpdate()
            .replace('%values%', this.updateValues(object))
            .replace('%conditions%', this.updateConditions(conditions));
        return query
    }

    updateValues(object) {
        if (!object) return '';
        return Object.keys(object).map(key => `${key} = ?`).join(', ');
    }

    updateConditions(conditions) {
        if (!conditions) return ' 1 = 1';
        return Object.keys(conditions).map(key => `${key} = ?`).join(' and ');
    }

    updateDataCondition(conditions) {
        if (!conditions) return [];
        return this.saveDataObject(conditions);
    }

    getQueryUpdate(target) {
        var _target = target || this.target;
        return 'UPDATE  ' + _target + ' SET %values% WHERE %conditions% ';
    }

    ////// UPDATE /////////


    ////// DELETE /////////
    deleteQuery(conditions) {
        var query = this.getQueryDelete()
            .replace('%values%', this.deleteDataConditions(conditions));
        return query;
    }

    deleteDataConditions(conditions) {
        if (!conditions) return '1 = 1';
        return Object.keys(conditions).map(key => key + ' = ?').join(' and ');
    }

    getQueryDelete(target) {
        var _target = target || this.target;
        return 'DELETE FROM  ' + _target + ' WHERE %values%';
    }
    ////// DELETE /////////

    ////// SELECT /////////
    getQuery(columns, condition) {
        var query = this.getQuerySelect()
            .replace('%columns%', this.getQuerySelectColumns(columns))
            .replace('%conditions%', this.getConditionSelect(condition))
            .replace('%aggregates%', this.getAggregateSelect(condition));
        return query;
    }

    getQuerySelect(target) {
        var _target = target || this.target;

        return 'SELECT %columns% FROM ' + _target + ' WHERE %conditions% %aggregates% ';
    }

    getQuerySelectColumns(columns) {
        if (!columns || !(columns instanceof Array) || !columns.length) return ' * ';

        return columns.join(', ');
    }

    getConditionSelect(conditions) {
        if (!conditions) {
            return '1 = 1';
        }

        let mapKeysConditions = Object.keys(conditions)
            .filter(key => this.KEYS_NO_CONDITION.indexOf(key) === -1)
            .map(key => key)

        let queryCondition = mapKeysConditions.map(key => `${key} = ?`).join(' and ');
        return queryCondition;
    }

    getAggregateSelect(conditions) {

        if (!conditions)
            return '';

        let objAggregates = Object.keys(conditions)
            .filter(key => this.KEYS_NO_CONDITION.indexOf(key) !== -1)
            .map(key => key)


        let key = null;
        let sqlKey = null;
        let queryAggregates = '';
        let conditionValue;
        for (let i = 0; i < objAggregates.length; i++) {
            key = objAggregates[i];
            if (!key) continue;

            key = key.trim();
            sqlKey = this.OBJECT_NOT_CONDITION[key];

            conditionValue = conditions[key];

            if (key === 'LIMIT') {
                queryAggregates += ` ${sqlKey} ${conditionValue.join(', ')}`
            } else if (key === 'ORDER') {
                conditionValue = conditionValue || [];
                queryAggregates += ` ${sqlKey} ${conditionValue.map(order => `${order[0]} ${order[1] || ' asc'}`).join(', ')}`
            } else if (key === 'GROUP') {
                conditionValue = conditionValue || [];
                queryAggregates += ` ${sqlKey} ${conditionValue.join(', ')}`
            } else if (key === 'HAVING') {
                conditionValue = conditionValue || [];
                queryAggregates += ` ${sqlKey} ${conditionValue.join(' and')}`
            }
        }

        return queryAggregates;
    }

    ////// SELECT /////////

}

module.exports = MySqlQueryFactory;
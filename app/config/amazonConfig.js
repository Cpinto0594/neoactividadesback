const aws = require('aws-sdk');
const appConfig = require('./appConfig')
const fs = require('fs');

module.exports = class AmazonConfig {


    configureBucketS3UserImagesProfile() {
        let json = require('../../aws_acckey.json');

        aws.config.update({
            accessKeyId: json.ACCKEY_ID,
            secretAccessKey: json.ACCKEY_PSS,
            region: appConfig.AMAZON_OHIO_REGION
        })

        return this;
    }

    configureBucketS3PostImages() {
        let json = require('../../aws_acckey.json');

        aws.config.update({
            accessKeyId: json.ACCKEY_ID,
            secretAccessKey: json.ACCKEY_PSS,
            region: appConfig.AMAZON_OHIO_REGION
        })

        return this;
    }

    configureBucketS3PostAttachments() {
        let json = require('../../aws_acckey.json');

        aws.config.update({
            accessKeyId: json.ACCKEY_ID,
            secretAccessKey: json.ACCKEY_PSS,
            region: appConfig.AMAZON_OHIO_REGION
        })

        return this;
    }

}
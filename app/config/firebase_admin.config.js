var admin = require('firebase-admin');
const dotenv = require('dotenv');
dotenv.config();


module.exports = class FirebaseConfig {


    static initializeApp() {
        admin.initializeApp({
            credential: admin.credential.applicationDefault()
        });
    }

    static sendMessage(message) {
        admin.messaging().send(message);
    }


}

const dotenv = require('dotenv');
dotenv.config();


module.exports = {
  //APP
  PORT: process.env.PORT,
  MOBILE_APP_VERSION: '0.1.1',
  MOBILE_APP_APP_VERSION_HEADER: 'X-Device-App-Version',
  MOBILE_APP_APP_KEY_HEADER: 'X-Device-App-Key',

  //AMAZON
  AMAZON_OHIO_REGION: 'us-east-2',
  AMAZON_APP_BUCKET: 'com.activitycontrol.bucket',
  AMAZOM_PROFILE_IMAGES_FOLDER: '/profile_pictures',
  AMAZON_POST_IMAGES_BUCKET: '/socialposts/{post}/images',
  AMAZON_POST_COMMENT_IMAGES_BUCKET: '/socialposts/{post}/comments/{comment}/comments_images',
  AMAZON_POST_ATTACHMENTS_BUCKET: '/socialposts/{post}/attachments',
  AMAZON_POST_COMMENTS_ATTACHMENTS_BUCKET: '/socialposts/{post}/comments/{comment}/comments_attachment',
  AMAZON_GROUP_PROFILE_PICTURE: '/socialgroups/{group}/picture',
  //FIREBASE
  FIREBASE_MESSAGING_URL: 'https://fcm.googleapis.com/fcm/send',
  FIREBASE_AUTHORIZATION_HEADER: 'key=AAAAsyeghxg:APA91bGzyql57NS8iOuIMDNA7hr6XQJq-OfQXfJr3f4NBgaj4jyPIHQS209NACrO1GUgbAnDjLx_usgo-OIF9sQGNkLrE5PwHdcwgSt6twj1bSC0F9t96FUxHfwY4TB3FPIrroh5LbMP',
  FIREBASE_APPUPDATE_TOPIC: '/topics/appUpdate',


  //DATABASE
  AWS_RDS_DATABASE_URL: process.env.AWS_RDS_DATABASE_URL,
  AWS_RDS_DATABASE_USER: process.env.AWS_RDS_DATABASE_USER,
  AWS_RDS_DATABASE_PASS: process.env.AWS_RDS_DATABASE_PASS,
  AWS_RDS_DATABASE_DATABASE: process.env.AWS_RDS_DATABASE_DATABASE

};
var socialPostServices = new (require('../../apiServices/socialpost/socialpost.services'))
var socialPostIMagesServices = new (require('../../apiServices/socialpost/socialpostimages.services'))
var uuid = require('uuid/v4')
var axios = require('axios');
var json = require('./post.json');
module.exports = class MergePosts {

    constructor() {
    }

    async init() {
        console.log('mergeBegin')
        await socialPostServices.repository.begin();

        try {
            let arrusuarios = [
                {
                    id: 13,
                    user: 'CPINTO'
                },
                {
                    id: 14,
                    user: 'KGAITAN'
                }, {
                    id: 17,
                    user: 'YMARIN'
                },
                {
                    id: 18,
                    user: 'JAYOLA'
                }
            ]

            for (let a = 0; a < 5; a++) {
                console.log('Iteration #' + (a + 1))
                for (let post in json.posts) {

                    let user = arrusuarios[Math.floor(Math.random() * (+arrusuarios.length - +1) + +1)]

                    let item = json.posts[post];

                    let response = await socialPostServices.save({
                        post_user_owner_id: user.id,
                        post_user_owner: user.user,
                        post_created_at: item.created_at,
                        post_published_at: item.created_at,
                        post_updated_at: item.updated_at,
                        post_content: item.excerpt,
                        post_type: 'post',
                        post_parent_uuid: null,
                        state: 'A',
                        uuid: uuid()
                    });

                    //let inserted = await socialPostServices.get([], {post_content:item.excerpt});
                    await socialPostServices.query(`insert into ${socialPostIMagesServices.collection} 
                            ( post_id , image_url , image_format, image_mime ,image_width , image_height ,
                             image_order , state, image_store_name , image_name )
                             values( ?, ?, ? , ? , ? , ? , ?, ? ,? ,?)` ,
                        [
                            response.insertId,
                            item.feature_image,
                            item.feature_image.substring(item.feature_image.lastIndexOf('.') + 1, item.feature_image.length),
                            'image/' + item.feature_image.substring(item.feature_image.lastIndexOf('.') + 1, item.feature_image.length),
                            300,
                            300,
                            1,
                            'A',
                            item.feature_image.substring(item.feature_image.lastIndexOf('/') + 1, item.feature_image.length),
                            item.feature_image.substring(item.feature_image.lastIndexOf('/') + 1, item.feature_image.length)
                        ]);



                }
            }

            console.log('mergeEnd')
            socialPostServices.repository.commit();

        } catch (error) {
            console.log(error)
            socialPostServices.repository.rollback();
        }





    }


}
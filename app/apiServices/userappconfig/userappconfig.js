var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');

class UserAppConfigurationServices {

    constructor() {
        this.collection = tables.UserAppConfig;
        this.repository = new MySqlRepository(tables.UserAppConfig);
    }

    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }
    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }
    query(query, data) {
        return this.repository.query(query, data);
    }

    async  merge(config) {
        var dataget = config.id ? { id: config.id } : { user_name: config.user_name };
        var _config = await this.get([], dataget);
        //Si configuracion existe lo actualizamos
        if (_config && _config[0] && _config[0].id) {
            var configID = _config[0].id;
            this.update(config, { id: configID });
            config.id = configID;
            return config;
        } else {
            //Si usuario no existe, lo creamos
            var _saveData = await this.save(config);
            config.id = _saveData.insertId;
            return config;
        }
    }

}


module.exports = UserAppConfigurationServices;




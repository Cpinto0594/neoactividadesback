var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');

class ActividadesServices {

    constructor() {
        this.collection = tables.Actividades;
        this.repository = new MySqlRepository(tables.Actividades);

    }


    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }

    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }

    export(object) {
        if (!object) {
            return Promise.reject({ success: false, message: 'No info provided' });
        }
        //dia , empresa, proyecto , usuario
        var query = 'call Log_Actividades_Export( ? )';
        var data = [
            typeof object !== 'string' ? JSON.stringify(object) : object
        ];
        return this.repository.query(query, data);
    }

    usuariosSinActividades(data) {

        return new Promise(async (succ, fail) => {

            try {
                let date = data.date;
                let query = 'call Usuarios_Sin_Actividades_Dia( ? )';

                let response = await this.repository.query(query, [date]);
                succ(response[0]);
            } catch (error) {
                fail(error);
            }

        });

    }


    dashboardChartProyectos(data) {

        return new Promise(async (succ, fail) => {

            try {
                let usuario = data.usuario;
                let date = data.date;
                let periodicidad = data.periodicidad;
                let query = 'call UserDashBoard_Chart_Proyectos( ? , ? , ?  )';

                let response = await this.repository.query(query, [usuario, periodicidad, date]);
                succ(response[0]);
            } catch (error) {
                fail(error);
            }

        });
    }

    dashboardChartEmpresas(data) {

        return new Promise(async (succ, fail) => {

            try {
                let usuario = data.usuario;
                let date = data.date;
                let periodicidad = data.periodicidad;
                let query = 'call UserDashBoard_Chart_Empresas( ? , ? , ?  )';

                let response = await this.repository.query(query, [usuario, periodicidad, date]);
                succ(response[0]);
            } catch (error) {
                fail(error);
            }

        });
    }

    dashboardEmpresasHoras(data) {

        return new Promise(async (succ, fail) => {

            try {
                let usuario = data.usuario;
                let date = data.date;
                let periodicidad = data.periodicidad;
                let query = 'call UserDashBoard_Horas_Empresa( ? , ? , ?  )';

                let response = await this.repository.query(query, [usuario, periodicidad, date]);
                succ(response[0]);
            } catch (error) {
                fail(error);
            }

        });
    }


    dashboardProyectosHoras(data) {

        return new Promise(async (succ, fail) => {

            try {
                let usuario = data.usuario;
                let date = data.date;
                let periodicidad = data.periodicidad;
                let query = 'call UserDashBoard_Horas_Proyecto( ? , ? , ?  )';

                let response = await this.repository.query(query, [usuario, periodicidad, date]);
                succ(response[0]);
            } catch (error) {
                fail(error);
            }

        });
    }



    dashboard(data) {
        return new Promise(async (succ, fail) => {

            try {
                await this.repository.begin();
                let data_proyectos = await this.dashboardChartProyectos(data);
                let data_empresas = await this.dashboardChartEmpresas(data);
                let data_horas_empresas = await this.dashboardEmpresasHoras(data);
                let data_horas_proyectos = await this.dashboardProyectosHoras(data);
                await this.repository.commit();
                succ({
                    chartEmpresas: data_empresas,
                    chartProyectos: data_proyectos,
                    horasEmpresas: data_horas_empresas,
                    horasProyectos: data_horas_proyectos
                });

            } catch (error) {
                await this.repository.rollback();
                fail(error);
            }
        });
    }

    autocomplete(query, usuario) {
        return new Promise(async (succ, fail) => {

            try {

                let response = await this.repository.query(`select distinct descripcion from ${this.collection} 
                where lower(descripcion) like lower('${query}%') and usuario = '${usuario}'`);
                succ(response);

            } catch (error) {
                fail(error);
            }
        });
    }


    createAll(object) {

        return new Promise(async (succ, fail) => {
            let data = object;
            if (data instanceof Array) {

                try {

                    await this.repository.begin();


                    for (let actividad of data) {
                        let result = await this.save(actividad)
                        actividad.id = result.insertId;
                    }

                    await this.repository.commit();
                    succ(data);

                } catch (error) {
                    await this.repository.rollback();
                    fail(error);
                }
            } else fail(Error('No Array data sent'));
        })

    }

}

module.exports = ActividadesServices;


var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');

class ProyectosServices {

    constructor() {
        this.collection = tables.Proyectos;
        this.repository = new MySqlRepository(tables.Proyectos);
    }

    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }

    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }

    query(query, conditions) {
        return this.repository.query(query, conditions);
    }
}


module.exports = ProyectosServices;




var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');


class PositionsSevices {

    constructor() {
        this.collection = tables.Positions;
        this.repository = new MySqlRepository(tables.Positions);
    }

    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }

    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }

    query(query, conditions) {
        return this.repository.query(query, conditions);
    }
}

module.exports = PositionsSevices;

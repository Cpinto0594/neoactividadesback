var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');


class SocialLikesServices {

    constructor() {
        this.collection = tables.SocialPostLikes;
        this.repository =  new MySqlRepository(tables.SocialPostLikes);
    }

    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }
    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }
    query(query, data) {
        return this.repository.query(query, data);
    }

    countPostLikes(postId, postType) {

        let query = `select count(1) count from ${tables.SocialPostLikes} where post_id = ? and like_type = ? and state = 'A'`;
        return this.query(query, [postId, postType])

    }



}


module.exports = SocialLikesServices;




var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');


class SocialPostAttachmentsServices {

    constructor() {
        this.collection = tables.SocialPostAttachment;
        this.repository = new MySqlRepository(tables.SocialPostAttachment);
    }

    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }
    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }
    query(query, data) {
        return this.repository.query(query, data);
    }


    findAttachmentsByPostId(postid) {
        return this.repository.get([], { post_id: postid })
    }


}


module.exports = SocialPostAttachmentsServices;




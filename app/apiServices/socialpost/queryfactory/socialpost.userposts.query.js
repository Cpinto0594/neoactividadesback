const SocialPostQueryFactory = require("./SocialPostQueryFactory");
const tables = require('../../../persistence/Tables');
const UserServices = require('../../usuarios/usuarios.services');
const SocialPostServices = require('../socialpost.services');
const TransactionProvider = require('../../../persistence/Mysql/MysqlTransactionProvider');

class UserSocialPostsFactory extends SocialPostQueryFactory {

    findPosts = async (req) => {

        let userServices = new UserServices;
        let socialPostServices = new SocialPostServices;
        let Transaction = new TransactionProvider

        try {

            await Transaction.beginTransaction()

            await userServices.repository.setCurrentTransaction(Transaction);
            await socialPostServices.repository.setCurrentTransaction(Transaction);


            let page = req.query.page || 1;
            let max_rows = req.query.max_rows || 10;
            let queries = req.query.include;
            let showImages = false;
            let showAttachments = false;
            let userRequesting = req.query.user_name;
            let userPostOwner = req.params.userUuid;

            if (queries) {

                let toShow = queries.split(',');
                showImages = toShow.indexOf('images') !== -1;
                showAttachments = toShow.indexOf('attachments') !== -1;
            }

            let page_start = page === 1 ? (page - 1) : ((page - 1) * max_rows);

            let userFilter = '0 user_liked';

            if (userRequesting) {
                let user = await userServices.findByUserName(userRequesting);
                userFilter = `( select ifnull( count(id) , 0 ) count
                                 from ${tables.SocialPostLikes} likes
                                where likes.like_type = 'post' 
                                  and likes.post_id = post.id  
                                  and likes.like_user_id = ${user.id} 
                                  and likes.state = 'A' ) user_liked`;
            }

            let userPosts = await userServices.findByUuid(userPostOwner);
            if ( !userPosts ) return null;

            let query = `
                        select posts.uuid ,
                            posts.post_created_at ,
                            posts.id ,
                            posts.post_updated_at, posts.post_content,
                            posts.post_type, posts.post_user_owner_id,
                            ifnull( posts.post_comments , 0 ) post_comments ,
                            ifnull( posts.post_likes , 0 ) post_likes ,
                            ifnull( posts.user_liked , 0 ) user_liked
                            
                        from (
                                select posts.uuid ,
                                    post.post_created_at ,
                                    post.id ,
                                    post.post_updated_at, post.post_content,
                                    post.post_type, post.post_user_owner_id ,
                                    ( select count(id) from ${tables.SocialPost} comm where comm.post_type = 'comment' and comm.post_parent_uuid = posts.uuid and comm.state = 'A') post_comments ,
                                    ( select count(id) from ${tables.SocialPostLikes} likes where likes.like_type = 'post' and likes.post_id = post.id and likes.state = 'A' ) post_likes ,
                                    ${userFilter}
                                from (
                                        SELECT
                                                post.uuid, IFNULL(MAX(comm.post_created_at),'1900-01-01 00:00:00') created
                                                FROM (SELECT uuid 
                                                        FROM ${tables.SocialPost} 
                                                        WHERE post_type = 'post'
                                                              and state = 'A'
                                                              and post_user_owner_id = ${userPosts.id}
                                                        ORDER BY post_created_at DESC 
                                                        LIMIT ${page_start} ,${max_rows} 
                                                    ) post
                                                LEFT JOIN ${tables.SocialPost} comm ON post.uuid = comm.post_parent_uuid
                                                GROUP BY post.uuid
                                    ) posts
                                join ${tables.SocialPost} post on post.uuid = posts.uuid 
                                    and post.post_type = 'post'  
                                    and state = 'A'
                                ) posts
                        order by posts.post_created_at desc
                `;

            let posts = await socialPostServices.query(query, {});

            let authors = {};
            //Find Authors
            for (let post of posts) {
                if (!authors[post.post_user_owner_id]) {
                    authors[post.post_user_owner_id] = await socialPostServices.findAuthorById(post.post_user_owner_id);
                    authors[post.post_user_owner_id] = authors[post.post_user_owner_id][0]
                }
                post.author = authors[post.post_user_owner_id]
                delete post["post_user_owner_id"]

                //Find Images
                if (showImages) {
                    post.images = await socialPostServices.findImages(post.id);
                }

                //Find Attachments
                if (showAttachments) {
                    post.attachments = await socialPostServices.findAttachment(post.id);
                }

                delete post.id;
                post.user_liked = !!post.user_liked
            }

            await Transaction.commitTransaction()

            return posts;

        } catch (error) {
            await Transaction.rollbackTransaction()
            throw error
        }

    }
}

module.exports = UserSocialPostsFactory;
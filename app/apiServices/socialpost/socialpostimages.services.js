var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');


class SocialPostServices {

    constructor() {
        this.collection = tables.SocialPostImages;
        this.repository = new MySqlRepository(tables.SocialPostImages);
    }

    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }
    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }
    query(query, data) {
        return this.repository.query(query, data);
    }


    findImagesByPostId(postid) {
        return this.repository.get([], { post_id: postid })
    }


}


module.exports = SocialPostServices;




var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');
const awsConfig = require('../../config/amazonConfig');
const appConfig = require('../../config/appConfig');
const aws = require('aws-sdk');


class SocialPostServices {

    constructor() {
        this.collection = tables.SocialPost;
        this.repository = new MySqlRepository(tables.SocialPost);
    }

    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }
    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }
    query(query, data) {
        return this.repository.query(query, data);
    }

    findPosts(post_parent_id, condition, postType, includeImages, includeAttachment, userName) {

        return new Promise(async (succ, fail) => {
            await this.repository.begin();
            try {

                let userFilter = '0 user_liked ';
                let user = await this.findUserByUserName(userName);
                user = user[0];

                let post_parent_query =
                    postType === 'post' ?
                        post_parent_id ? `and uuid = '${post_parent_id}'` //Si estoy consultando un post en especifico
                            : ''
                        : `and post_parent_uuid = '${post_parent_id}'`; //Si estoy consultando los hijos de post/comentario


                

                await this.repository.commit();

                return succ(posts);
            } catch (error) {
                await this.repository.rollback();
                fail(error);
            }
        })


    }

    async savePostImage(data) {
        return this.query(`INSERT INTO ${tables.SocialPostImages} (image_name, image_format, image_mime,  image_url, image_width, 
                            image_height, image_order, post_id, image_store_name)
                             VALUES(?, ?, ?, ?, ?, ?, ?, ?, ? ) `,
            this.repository.saveDataObject(data)
        );
    }

    async savePostAttachment(data) {

        return this.query(
            `INSERT INTO ${tables.SocialPostAttachment} (attachment_name, attachment_order, attachment_size,  attachment_url, 
                        attachment_format, attachment_mime , post_id, attachment_store_name)
             VALUES(?, ?, ?, ?, ?, ? , ? , ? ) `,
            this.repository.saveDataObject(data)
        );
    }

    async savePostImagesAwsS3(postUuid, image) {

        let awsFunctions = new awsConfig;
        awsFunctions.configureBucketS3PostImages();
        let bucketName = appConfig.AMAZON_APP_BUCKET + appConfig.AMAZON_POST_IMAGES_BUCKET.replace('{post}', postUuid)


        var s3 = new aws.S3()
        return s3.putObject({
            Bucket: bucketName,
            Body: image.data,
            Key: image.name,
            ACL: 'public-read'
        })
            .promise()
            .then(response => {
                console.log(`done! - `, response);
                let imagePath = s3.getSignedUrl('getObject',
                    {
                        Bucket: bucketName,
                        Key: image.name,
                    });

                return Promise.resolve({
                    temp_image_path: imagePath,
                    image_path: imagePath.substring(0, imagePath.indexOf('?'))
                })
            });


    }

    async savePostCommentImagesAwsS3(postUuid, commentUuid, image) {

        let awsFunctions = new awsConfig;
        awsFunctions.configureBucketS3PostImages();
        let bucketName = appConfig.AMAZON_APP_BUCKET + appConfig.AMAZON_POST_COMMENT_IMAGES_BUCKET
            .replace('{post}', postUuid)
            .replace('comment', commentUuid)


        var s3 = new aws.S3()
        return s3.putObject({
            Bucket: bucketName,
            Body: image.data,
            Key: image.name,
            ACL: 'public-read'
        })
            .promise()
            .then(response => {
                console.log(`done! - `, response);
                let imagePath = s3.getSignedUrl('getObject',
                    {
                        Bucket: bucketName,
                        Key: image.name,
                    });

                return Promise.resolve({
                    temp_image_path: imagePath,
                    image_path: imagePath.substring(0, imagePath.indexOf('?'))
                })
            });


    }

    async savePostAttachmentsAwsS3(postUuid, attachment) {

        let awsFunctions = new awsConfig;
        awsFunctions.configureBucketS3PostAttachments();
        let bucketName = appConfig.AMAZON_APP_BUCKET + appConfig.AMAZON_POST_ATTACHMENTS_BUCKET.replace('{post}', postUuid)


        var s3 = new aws.S3()
        return s3.putObject({
            Bucket: bucketName,
            Body: attachment.data,
            Key: attachment.name,
            ACL: 'public-read'
        })
            .promise()
            .then(response => {
                console.log(`done! - `, response);
                let attachmentPath = s3.getSignedUrl('getObject',
                    {
                        Bucket: bucketName,
                        Key: attachment.name,
                    });

                return Promise.resolve({
                    temp_attachment_path: attachmentPath,
                    attachment_path: attachmentPath.substring(0, attachmentPath.indexOf('?'))
                })
            });


    }

    async savePostCommentAttachmentsAwsS3(postUuid, commentUuid, attachment) {

        let awsFunctions = new awsConfig;
        awsFunctions.configureBucketS3PostAttachments();

        let bucketName = appConfig.AMAZON_APP_BUCKET + appConfig.AMAZON_POST__COMMENTS_ATTACHMENTS_BUCKET
            .replace('{post}', postUuid)
            .replace('comment', commentUuid)


        var s3 = new aws.S3()
        return s3.putObject({
            Bucket: bucketName,
            Body: attachment.data,
            Key: attachment.name,
            ACL: 'public-read'
        })
            .promise()
            .then(response => {
                console.log(`done! - `, response);
                let attachmentPath = s3.getSignedUrl('getObject',
                    {
                        Bucket: bucketName,
                        Key: attachment.name,
                    });

                return Promise.resolve({
                    temp_attachment_path: attachmentPath,
                    attachment_path: attachmentPath.substring(0, attachmentPath.indexOf('?'))
                })
            });


    }

    findAuthorById(authorId) {
        return this.repository.query(
            `select nombre_completo, pic , usuario from ${tables.Usuarios} where id = ? `,
            [authorId]
        );
    }

    findUserByUserName(userName) {
        return this.repository.query(
            `select id , nombre_completo, pic , usuario from ${tables.Usuarios} where usuario = ? `,
            [userName]
        );
    }

    findUserByUserUuid(userUuid) {
        return this.repository.query(
            `select id , nombre_completo, pic , usuario from ${tables.Usuarios} where uuid = ? `,
            [userUuid]
        );
    }

    findImages(id) {
        return this.repository.query(
            `select image_url, image_format, image_width, image_height, image_order, id ,
                    image_name, image_store_name 
                    from ${tables.SocialPostImages} where post_id = ? order by id`,
            [id]
        );
    }

    findAttachment(id) {
        return this.repository.query(
            `select attachment_name, attachment_store_name, attachment_url,attachment_format, attachment_size, attachment_order, id 
                    from ${tables.SocialPostAttachment} where post_id = ? order by id`,
            [id]
        );
    }


}


module.exports = SocialPostServices;




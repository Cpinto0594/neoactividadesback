var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');
const aws = require('aws-sdk');
const appConfig = require('../../config/appConfig');
const awsConfig = require('../../config/amazonConfig');
var userAppConfig = new (require('../../apiServices/userappconfig/userappconfig'));


class UsuariosServices {

    constructor() {
        this.collection = tables.Usuarios;
        this.repository = new MySqlRepository(tables.Usuarios);
    }


    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }
    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }
    query(query, data) {
        return this.repository.query(query, data);
    }
    desAsociarProyectos(usuarioId) {
        if (!usuarioId)
            throw new Error('Debe relacionar usuario a desasociar proyectos');

        var queryDeleteProyectos = 'DELETE FROM ' + tables.ProyectosUsuarios + ' WHERE usuario_id = ?';
        return this.query(queryDeleteProyectos, [usuarioId]);
    }

    asociarProyectos(usuarioId, proyectos) {
        if (!usuarioId || (!proyectos || !(proyectos instanceof Array) || !proyectos.length))
            throw new Error('Debe establecer los parámetros correctos para asociar proyectos a usuario');

        var querySaveProyectos = 'INSERT INTO ' + tables.ProyectosUsuarios + '( usuario_id , proyecto_id ) values ?';
        var data = proyectos.map(proyecto => [usuarioId, proyecto]);
        return this.query(querySaveProyectos, [data]);
    }

    desAsociarEmpresas(usuarioId) {
        if (!usuarioId)
            throw new Error('Debe relacionar usuario a desasociar empresas');

        var queryDeleteEmpresas = 'DELETE FROM ' + tables.EmpresasUsuarios + ' WHERE usuario_id = ?';
        return this.query(queryDeleteEmpresas, [usuarioId]);
    }
    desAsociarEmpresa(usuarioId, empresaId) {
        if (!usuarioId || !empresaId)
            throw new Error('Debe relacionar datos para desasociar empresas');

        var queryDeleteEmpresas = 'DELETE FROM ' + tables.EmpresasUsuarios + ' WHERE usuario_id = ? and empresa_id = ? ';
        return this.query(queryDeleteEmpresas, [usuarioId, empresaId]);
    }
    desAsociarProyecto(usuarioId, proyectoId) {
        if (!usuarioId || !proyectoId)
            throw new Error('Debe relacionar datos para desasociar proyecto');

        var queryDeleteEmpresas = 'DELETE FROM ' + tables.ProyectosUsuarios + ' WHERE usuario_id = ? and proyecto_id = ? ';
        return this.query(queryDeleteEmpresas, [usuarioId, proyectoId]);
    }
    asociarEmpresas(usuarioId, empresas) {
        if (!usuarioId || (!empresas || !(empresas instanceof Array)))
            throw new Error('Debe establecer los parámetros correctos para asociar empresas a usuario');

        var querySaveEmpresas = 'INSERT INTO ' + tables.EmpresasUsuarios + '( usuario_id , empresa_id  ) values ?';
        var data = empresas.map(empresa => [usuarioId, empresa]);
        return this.query(querySaveEmpresas, [data]);
    }

    async  conAsociaciones(usuarioId) {
        var data = await this.get([], { id: usuarioId });
        var asociaciones = await this.getAsociaciones(usuarioId);
        return {
            usuario: data,
            empresas: asociaciones.empresas,
            proyectos: asociaciones.proyectos
        }
    }

    async login(data) {
        return new Promise(async (succ, fail) => {

            try {
                ///
                //1- Usuario
                //2- Clave 
                //3- Movil Notificatciones Token
                //4- Web Notificaciones Token
                //5- Dispoisitivo Logueo
                var _query = 'call Login( ?,?,?,?,? )';
                var dataLogin = [data.usuario, data.clave, data.movilNT, data.webNT, data.loggedFrom]
                var respuesta = await this.query(_query, dataLogin);
                respuesta = respuesta[0][0];

                if (!respuesta.result) {
                    throw Error('No se obtuvo respuesta correcta del Login.');
                }

                respuesta = JSON.parse(respuesta.result);
                let asociaciones = await this.getAsociaciones(respuesta.userId);
                respuesta.userProyectos = asociaciones.proyectos;
                respuesta.userEmpresas = asociaciones.empresas;

                succ(respuesta);
            } catch (e) {
                fail(e);
            }
        });
    }

    async  getAsociaciones(usuarioId) {
        var data = {};
        var queryGetEmpresas = 'SELECT eu.empresa_id , e.descripcion FROM  ' + tables.EmpresasUsuarios + ' eu JOIN ' + tables.Empresas + ' e on e.id = eu.empresa_id WHERE usuario_id = ? ';
        var empresas = await this.query(queryGetEmpresas, [usuarioId]);
        data.empresas = empresas;

        var queryGetProyectos = 'SELECT eu.proyecto_id , e.descripcion FROM  ' + tables.ProyectosUsuarios + ' eu JOIN ' + tables.Proyectos + ' e on e.id = eu.proyecto_id WHERE usuario_id = ? ';
        var proyectos = await this.query(queryGetProyectos, [usuarioId]);
        data.proyectos = proyectos;

        return data;
    }

    async  merge(usuario) {
        var dataget = usuario.id ? { id: usuario.id } : { usuario: usuario.usuario };
        var _usuario = await this.get([], dataget);
        //Si usuario existe lo actualizamos
        if (_usuario && _usuario[0] && _usuario[0].id) {
            var usuarioID = _usuario[0].id;
            this.update(usuario, { id: usuarioID });
            usuario.id = usuarioID;
            usuario.clave = null;
            return usuario;
        } else {
            //Si usuario no existe, lo creamos
            var _saveData = await this.save(usuario);
            usuario.id = _saveData.insertId;
            usuario.clave = null;
            return usuario;
        }
    }

    async  crearYAsociar(data) {
        if (!data) return;

        var usuario = data.usuario;
        var proyectos = data.proyectos;
        var empresas = data.empresas;
        var configuracion = data.configuracion;

        console.log('Iniciando Transaccion');
        var conn = await this.repository.begin();
        if (!conn) {
            return null;
        }
        console.log('Se inicia transaccion')
        try {

            var usuario = await this.merge(usuario);
            console.log('Se guarda Usuario ')
            if (proyectos && proyectos.length) {
                await this.desAsociarProyectos(usuario.id);
                console.log('Se desasocia proyecto ')
                await this.asociarProyectos(usuario.id, proyectos);
            }
            if (empresas && empresas.length) {
                console.log('Se asocia proyecto ')
                await this.desAsociarEmpresas(usuario.id);
                console.log('Se desasocia empresa ')
                await this.asociarEmpresas(usuario.id, empresas);
            }
            console.log('Se sasocia empresa ')

            if (configuracion) {
                await userAppConfig.merge(configuracion);
            }

            await this.repository.commit();
            console.log('commited ')



            return usuario;

        } catch (e) {
            throw this.handleError(e);
        }
    }


    async saveDeviceInfo(usuario, data) {
        return new Promise(async (succ, fail) => {
            try {
                var user = await this.get([], { usuario: usuario });

                if (!user || !user.length) {
                    throw Error(`No se encontró este usuario [ ${usuario} ]`);
                }
                user = user[0];
                user.system_information = data;
                await this.update(user, { id: user.id });
                succ(true);

            } catch (e) {
                fail(e);
            }
        });
    }

    saveImage(user, image) {
        let awsFunctions = new awsConfig;
        awsFunctions.configureBucketS3UserImagesProfile();

        var s3 = new aws.S3()
        return s3.putObject({
            Bucket: appConfig.AMAZON_APP_BUCKET + appConfig.AMAZOM_PROFILE_IMAGES_FOLDER,
            Body: image.data,
            Key: `image_profile_${user}`,
            ACL: 'public-read'
        })
            .promise()
            .then(response => {
                console.log(`done! - `, response);
                let imagePath = s3.getSignedUrl('getObject',
                    {
                        Bucket: appConfig.AMAZON_APP_BUCKET + appConfig.AMAZOM_PROFILE_IMAGES_FOLDER,
                        Key: `image_profile_${user}`
                    });

                return Promise.resolve({
                    temp_image_path: imagePath,
                    image_path: imagePath.substring(0, imagePath.indexOf('?')) + `?a=${new Date().getTime()}`
                })
            });


    }

    async findByUuid(uuid) {
        try {

            let response = await this.repository.get([], { uuid });
            return response[0];

        } catch (error) {
            console.log(error)
            return null;
        }
    }

    async findByUserName(usuario) {
        try {

            let response = await this.repository.get([], { usuario });
            return response[0];

        } catch (error) {
            console.log(error)
            return null;
        }
    }



    async handleError(ex) {
        console.log('RollingBack ')
        await this.repository.rollback();
        return ex;
    }

}


module.exports = UsuariosServices;




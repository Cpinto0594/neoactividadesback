var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');


class UserAppConfigurationServices {

    constructor() {
        this.collection = tables.SystemUserExcludedViews;
        this.repository = new MySqlRepository(tables.SystemUserExcludedViews);
    }

    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }
    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }
    query(query, data) {
        return this.repository.query(query, data);
    }

    async excludedViews(usuarioId) {

        let excluded = await this.get([], { user_id: usuarioId, estado: 'A' });

        let arrExcluded = [];

        if (excluded && excluded.length) {
            arrExcluded = excluded.map(view => view.view);
        }

        return arrExcluded;
    }


}


module.exports = UserAppConfigurationServices;




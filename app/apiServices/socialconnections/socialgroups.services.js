var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');
const awsConfig = require('../../config/amazonConfig');
const appConfig = require('../../config/appConfig');
const aws = require('aws-sdk');

class SocialConnectionGroupServices {

    constructor() {
        this.collection = tables.SocialGroups;
        this.repository = new MySqlRepository(tables.SocialGroups);

    }

    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }
    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }
    query(query, data) {
        return this.repository.query(query, data);
    }

    findUserGroups(userIdentifier, page) {



        let query = `   
                        select groupss.uuid, 
                                groupss.group_description,
                                groupss.group_photo ,
                                groups_a.nombre_completo group_owner,
                                groupss.group_privacy,
                                case when groups_a.owner_identifier = '${userIdentifier}' then 1 else 0 end is_group_owner ,
                                ( select count(1) from Social_Groups_Members where state = 'A' and group_id = groupss.id ) group_members
                            from (
                                    select groups_a.id , 
                                           us.nombre_completo ,
                                           us.uuid owner_identifier
                                    from (
                                            select id
                                                from Social_Groups 
                                                where state = 'A'
                                                order by group_created_at desc
                                                limit ${page[0]} , ${page[1]}
                                            ) groups_a
                                    join Social_Groups_Members memb on memb.group_id = groups_a.id and memb.group_member_role = 'ADMIN' 
                                    join Gen_Usuarios us on us.id = memb.group_member_id 
                                    ) groups_a
                            join Social_Groups groupss on groupss.id = groups_a.id
                            join Social_Groups_Members memb on memb.group_id = groups_a.id and memb.group_member_role in(  'MEMBER', 'ADMIN' ) and memb.state = 'A'
                            join Gen_Usuarios us on us.id = memb.group_member_id 
                            where us.uuid = '${userIdentifier}'
                            order by groupss.group_created_at
                        `;

        return this.repository.query(query, []);


    }


    saveMember(member) {
        let query = `insert into ${tables.SocialGroupsMembers} ( ${repository.getColumnsInsert(member)} ) values 
        ${repository.getValuesInsert(member)} `;
        return this.repository.query(
            query,
            this.repository.saveDataObject(member)
        )
    }

    saveGroupImagesAwsS3 = async (groupUuid, image) => {

        let awsFunctions = new awsConfig;
        awsFunctions.configureBucketS3PostImages();
        let bucketName = appConfig.AMAZON_APP_BUCKET + appConfig.AMAZON_GROUP_PROFILE_PICTURE.replace('{group}', groupUuid)

        var s3 = new aws.S3()
        return s3.putObject({
            Bucket: bucketName,
            Body: image.data,
            Key: image.name,
            ACL: 'public-read'
        })
            .promise()
            .then(response => {
                console.log(`done! - `, response);
                let imagePath = s3.getSignedUrl('getObject',
                    {
                        Bucket: bucketName,
                        Key: image.name,
                    });

                return Promise.resolve({
                    temp_image_path: imagePath,
                    image_path: imagePath.substring(0, imagePath.indexOf('?'))
                })
            });


    }

}


module.exports = SocialConnectionGroupServices;




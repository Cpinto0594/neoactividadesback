var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');

class SocialConnectionGroupMembersServices {

    constructor() {
        this.collection = tables.SocialGroupsMembers;
        this.repository = new MySqlRepository(tables.SocialGroupsMembers);
    }

    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }
    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }
    query(query, data) {
        return this.repository.query(query, data);
    }


    async findGroupMembersByUuid(groupUuid) {
        return await this.repository.query(
            `select us.uuid, us.nombre_completo, us.pic, us.email , mem.group_member_role
               from ${tables.Usuarios} us 
               join ${tables.SocialGroupsMembers} mem on mem.group_member_id = us.id 
               join ${tables.SocialGroups} grup on grup.id = mem.group_id 
              where grup.uuid = ?  `,
            [groupUuid]
        );
    }

}


module.exports = SocialConnectionGroupMembersServices;




var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');


class AppUpdatesServices {

    constructor() {
        this.collection = tables.AppUpdates;
        this.repository = new MySqlRepository(tables.AppUpdates);
    }

    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }
    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }
    query(query, data) {
        return this.repository.query(query, data);
    }

    checkUpdates() {

        return new Promise(async (succ, fail) => {

            try {

                let data = await this.get([], { estado: 'A' });
                data = data[0];

                succ(data);

            } catch (error) {
                fail(error)
            }

        });

    }
}


module.exports = AppUpdatesServices;




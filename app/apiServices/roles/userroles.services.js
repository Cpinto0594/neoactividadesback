var MySqlRepository = require('../../persistence/Mysql/MySqlRepository.js')
var tables = require('../../persistence/Tables');
var UserRolesServices = (require('./roles.services'));


class SystemUserRolesServices {

    constructor() {
        this.collection = tables.SystemUserRoles;
        this.repository = new MySqlRepository(tables.SystemUserRoles);
    }

    get(columns, conditions) {
        return this.repository.get(columns, conditions);
    }
    save(object) {
        return this.repository.save(object);
    }
    remove(object) {
        return this.repository.delete(object);
    }
    update(object, conditions) {
        return this.repository.update(object, conditions);
    }
    query(query, data) {
        return this.repository.query(query, data);
    }


    async findUserRoles(userId) {

        let rolesServices = new UserRolesServices;
        await rolesServices.repository.setCurrentTransaction(this.repository.getCurrentTransaction())

        let userRoles = await this.get([], { usuario_id: userId });
        if (!userRoles || !userRoles.length) {
            return [];
        }

        let arrUserRoles = [];
        for (let i = 0; i < userRoles.length; i++) {
            let rol = userRoles[i];

            let _rol = await rolesServices.get([], { id: rol.rol_id });
            _rol = _rol[0];
            if (_rol) {
                arrUserRoles.push({
                    user_id: userId,
                    rol_id: _rol.id,
                    descripcion: _rol.descripcion,
                    codigo: _rol.codigo
                });
            }
        }
        return arrUserRoles;
    }


}


module.exports = SystemUserRolesServices;




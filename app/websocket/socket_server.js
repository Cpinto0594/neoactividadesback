const WebSocket = require('ws');
const http = require('http');
const url = require('url');
const url_pattern = require('url-pattern')



class SocketServerWeb {

    constructor() {
        this.arrHttpServers = [];
        this.maxServers = 2;
        this.numberOfAliveServers = 0;
    }

    static getInstance() {
        if (!SocketServerWeb.INSTANCE) {
            SocketServerWeb.INSTANCE = new SocketServerWeb;
        }
        return SocketServerWeb.INSTANCE;
    }


    createServer(port, name, broadcaster) {
        //Verificamos puerto y nombre para el server
        if (!port || this.numberOfAliveServers === this.maxServers) return;
        if (!name) name = 'http_server_' + this.numberOfAliveServers + 1;

        //Si el servidor ya existe lanzamos lanzamos excepcion, si no, lo creamos en el pool
        let exist = this.arrHttpServers.find(httpS => (httpS.port === +port || httpS.name === name));
        if (exist) throw Error(`Server with this name ( ${name} ) or listening on port ( ${port} ) already exists.`);


        let newServer = new HttpServer(port, name);

        this.arrHttpServers.push(newServer);
        this.numberOfAliveServers++;

        return newServer;
    }

}

class HttpServer {


    constructor(port, name) {
        if (typeof arguments[0] !== 'string' &&
            (typeof arguments[0] === 'object' && !(port instanceof Array))) {
            this.server_port = port.port;
            this.server_name = port.name;
        } else {
            this.server_port = port;
            this.server_name = name;
        }

    }

    aceptedPaths(paths) {
        if (paths && paths instanceof Array && paths.length) {
            this.pathSubscriptions = paths;
            return this;
        }
    }

    validateAceptedRoutes() {
        if (!this.pathSubscriptions || !this.pathSubscriptions.length) throw Error('No paths to listen');
    }

    initAsBroadCaster() {
        this.server_broadcaster = true;
        this.__startListening();
    }

    initAsSingle() {
        this.server_broadcaster = false;
        this.__startListening();
    }


    __startListening() {
        this.validateAceptedRoutes();
        let serverInstance;

        try {

            this.__serverInstance = http.createServer();

            serverInstance = this.__serverInstance;

        } catch (error) {
            throw Error('Server cant be created, Reason: ' + error.message);
        }

        let paths = this.pathSubscriptions;

        let destroyerSocket = new WebSocket.Server({ noServer: true });


        serverInstance.on('upgrade', (request, sock, head) => {

            const pathname = url.parse(request.url).pathname;
            let aceptedPath = paths.some(path => shouldHandle(path, request));

            //Chequeamos si el path cliente está siendo manejado en este servidor
            if (!aceptedPath) {
                console.log(`Ìgnoring connection from ${pathname} destination not 
                allowed in server ${this.server_name}:${this.server_port}`);
                destroyerSocket.handleUpgrade(request, sock, head,
                    (ws) => {
                        ws.close();
                    });
                return;
            }

            //Check if this server contain any socketserver listening this incoming pathname
            let socket = this.__wsss.find(ssk => ssk.socketPath === pathname);

            //Si no existe este SocketServer Creado, Creamos uno
            if (!socket) {
                socket = new ServerSocket(pathname, this, this.server_broadcaster);
                socket.onIncomingConnection(request, sock, head);

                this.__wsss.push(socket)
            } else {
                //Dispatch incoming connection
                socket.onIncomingConnection(request, sock, head);
            }

        });

        this.__serverInstance.listen(this.server_port);


    }

}
HttpServer.prototype.__serverInstance = null;
HttpServer.prototype.__wsss = [];


class ServerSocket {

    constructor(path, server, broadcaster) {

        this.socket = new WebSocket.Server({
            noServer: true,
            path: path,
            autoAcceptConnections: false
        });
        this.socket.path = path;
        this.socketPath = path;
        this.server = server;
        this.socketId = this.createUniqueID();
        this.serverBroadCaster = broadcaster;
        this.arrClients = {};
        this.clientIds = [];
        this.initListeners();
        this.messageEmmiter = Promise.resolve;

    }


    initListeners() {


        this.socket.on('connection', (wsCliente, req) => {
            let clientId = this.createUniqueID();

            this.logServer(`Received incoming connection from clientId ${clientId}`);

            wsCliente.on('message', (message) => {
                this.logServer(`Received message ( ${message} ), ${this.serverBroadCaster ? 'Sending as BroadCaster' : 'Sending to client'}`);
                if (this.serverBroadCaster) {
                    for (let client in this.arrClients) {
                        if (client !== clientId && this.arrClients[client].readyState === WebSocket.OPEN) {
                            this.arrClients[client].send(message);
                        }
                    }
                } else {
                    wsCliente.send(`Message sent from server ${this.socketPath}: ${message} `);
                }

            });

            wsCliente.on('close', () => {

                this.logServer(`Client ${clientId} Closed Connection`);
                delete this.arrClients[clientId];

            })

            //Add Client To Set of Clients
            this.arrClients[clientId] = wsCliente;
        });

        let intervalid = setInterval(() => {
            this.logServer(`Number of connections: ${this.arrClients.length}`)
            if (!this.arrClients.length) clearInterval(intervalid)
        }, 1000)
    }

    logServer(message) {

        console.log('//////////////////////////////////////////////////////');
        console.log(`From SocketServer ${this.socketPath}-${this.socketId}: ${message}`);
        console.log('//////////////////////////////////////////////////////');
        console.log()

    }
    onMessageReveived(cb) {
        if (typeof cb !== 'function') return;
        this.messageEmmiter.then(msg => { cb(msg) });
    }

    createUniqueID() {
        const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        return s4() + s4() + '-' + s4();
    }


    onIncomingConnection(request, clientSocket, head) {
        this.socket.handleUpgrade(request, clientSocket, head,
            (ws) => {
                this.socket.emit('connection', ws, request);
            });
    }

}





const shouldHandle = (path, req) => {

    const url = require('url').parse(req.url).pathname;
    let pattern = new url_pattern(path);

    //Si no hay pattern que verificar validamos que concuerden las Url y el Path del servidor
    if (!pattern) {
        return url === path;
    }

    const match = pattern.match(url)

    if (!match) {
        return false;
    }
    if (path.indexOf(':') !== -1) {

        let keys = path.split('/')
            .filter(part => part.indexOf(':') !== -1)
            .map(part => part.replace(':', ''));

        if (!req.params) {
            req.params = {}
        }
        //Asignamos  cada key al request.params
        keys.forEach(key => { req.params[key] = match[key] });

        //Retornamos si la url concuerda
        let hasKeys = keys.every(part => !!(req.params[part]));
        return hasKeys;
    }
    return true;
}


module.exports = SocketServerWeb
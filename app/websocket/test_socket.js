const WebSocket = require('ws');


const logMessage = (message) => {
    console.log('////////////////////////////////////////////////////');
    console.log(`Client Message: ${message}`);
    console.log('////////////////////////////////////////////////////');
    console.log();
}

// const ws1 = new WebSocket('ws://localhost:8080/topic/notification/bar');
// ws1.on('open', () => {
//     logMessage('Connected Cliente 1');


//     setTimeout(() => {
//         ws1.send('Cliente 1 message ' + new Date())
//         //ws1.close();
//     }, 3000);
// })
// ws1.on('message', (message) => {
//     logMessage('Socket1 Received message -> ' + message);
// })
// ws1.on('close', () => {
//     logMessage('Close1')
// })


// const ws2 = new WebSocket('ws://localhost:8080/topic/notification/bar');
// ws2.on('open', () => {
//     logMessage('Connected Cliente 2');

//     setTimeout(() => {
//         ws2.send('Cliente 2 Message ' + new Date())
//     }, 5000);
// })
// ws2.on('message', (message) => {
//     logMessage('Socket2 Received message -> ' + message);
// })
// ws2.on('close', () => {
//     logMessage('Close2')
// })

// httpServer.listen(8080)
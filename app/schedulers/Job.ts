export default interface Job{
    expression();
    execute();
}
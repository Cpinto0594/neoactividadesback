const nodeScheduler = require('node-schedule');
let noActividadesScheduler = require('../schedulers/actividades/noActivitiesRegisteredCron');
let appUpdatesCron = require('../schedulers/actividades/appUpdateCron')


module.exports = {
    executeSchedulers: () => {

        noActividadesScheduler = new noActividadesScheduler;
        appUpdatesCron = new appUpdatesCron;
        //El cron se va a ejecutar Cada Hora desde las 7am hasta las 8 pm
        //Con el Timezone de Bogotá
        //Cada dia de Lunes a Sabado

        var rule = new nodeScheduler.RecurrenceRule();
        rule.dayOfWeek = new nodeScheduler.Range(1, 5); //De Lunes a Viernes
        rule.hour = [12, 18]; //A las 12m y 6pm
        rule.minute = 0;

        var schedulerNoActividadesDiario = nodeScheduler.scheduleJob({ rule: rule, tz: 'America/Bogota' }, () => {
            try {
                noActividadesScheduler.execute();
                console.log('Week Scheduler Triggering next Scheduler at ', schedulerNoActividadesDiario.nextInvocation().toString());

            } catch (error) {
                console.log('SCHEDULER_DIARIO::::::: No se pudo ejecutar Scheduler de envio notificaciones actividades Diarias', error.message);
            }
        });
        console.log('Week Scheduler Triggering next Scheduler at ', schedulerNoActividadesDiario.nextInvocation().toString());


        //Sábados
        rule = new nodeScheduler.RecurrenceRule();
        rule.dayOfWeek = 6; //Sabados
        rule.hour = 12; //A las 12m
        rule.minute = 0;

        var schedulerNoActividadesSabados = nodeScheduler.scheduleJob({ rule: rule, tz: 'America/Bogota' }, () => {
            try {
                noActividadesScheduler.execute();
                console.log('Weekend Scheduler Triggering next Scheduler at ', schedulerNoActividadesSabados.nextInvocation().toString());

            } catch (error) {
                console.log('SCHEDULER_FINSEMANA::::::: No se pudo ejecutar Scheduler de envio notificaciones actividades Sabado', error.message);
            }
        });
        console.log('Weekend scheduler Triggering next Scheduler at ', schedulerNoActividadesSabados.nextInvocation().toString());


        //APP Updates
        // rule = new nodeScheduler.RecurrenceRule();
        // rule.dayOfWeek = new nodeScheduler.Range(0, 6)
        // rule.minute = 0
        // rule.second = 0;

        // var schedulerAppUpdates = nodeScheduler.scheduleJob({ rule: rule, tz: 'America/Bogota' }, () => {
        //     try {
        //         appUpdatesCron.execute();
        //     } catch (error) {
        //         console.log('SCHEDULER_APPUPDATES::::::: No se pudo ejecutar Scheduler de envio notificaciones actualizacion de aplicacion', error.message);
        //     }
        // });
        // console.log( 'AppUpdatesScheduler Triggering next Scheduler at ', schedulerAppUpdates.nextInvocation().toString() )

    }

}
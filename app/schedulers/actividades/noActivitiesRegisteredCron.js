
var actividadesServices = new (require('../../apiServices/actividades/actividades.services'));
var axios = require('axios');
const config = require('../../config/appConfig');


class NoActivitiesRegisteredCron {
    constructor() {
    }


    execute() {

        try {

            (async () => {
                let data = await actividadesServices.usuariosSinActividades({ date: new Date() });
                let arrRequests = [];

                let arrUsers = data || [];
                arrUsers.forEach(user => {
                    let message = {
                        to: user.token,
                        data: {
                            title: 'Registro de Actividades Diarias',
                            body: user.texto,
                            priority: 'high'
                        },
                        priority: 'high'
                    }
                    arrRequests.push(this._sendMessages(message))
                });
                axios.all(arrRequests)
                    .then((a, b) => {
                    }, (error) => {
                        console.error(error)
                    }).catch((error) => {
                        console.error(error)
                    });
            })();

        } catch (error) {
            console.error('No se pudo enviar Notificaciones ' , error);
        }


    }

    _sendMessages(messageData) {

        return axios.post(config.FIREBASE_MESSAGING_URL,
            messageData,
            {
                headers: {
                    'Authorization': config.FIREBASE_AUTHORIZATION_HEADER,
                    'Content-Type': 'application/json'
                }
            });
    }

}

module.exports = NoActivitiesRegisteredCron;
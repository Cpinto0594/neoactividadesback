
var appUpdatesServices = new (require('../../apiServices/appupdates/appupdates.services'));
const config = require('../../config/appConfig');
const firebaseConfig = require('../../config/firebase_admin.config');

class AppUpdatesCron {
    constructor() {
    }


    execute() {

        try {

            (async () => {
                try {

                    let data = await appUpdatesServices.checkUpdates();

                    //Fix For Firebase -> Data must be string
                    Object.keys(data).forEach(key => {
                        data[key] = typeof data[key] === 'string' ? data[key] : ("" + data[key])
                    });
                    if (data) {
                        let message = {
                            topic: config.FIREBASE_APPUPDATE_TOPIC,
                            data: {
                                title: 'Notificación de Actualización de la Aplicación',
                                body: 'Ha sido lanzada una nueva versión de la aplicación',
                                priority: 'high',
                                TYPE: 'APP_UPDATE',
                                ...data
                            }
                        }

                        this._sendMessages(message)

                    }

                } catch (error) {
                    console.log('No se pudo enviar notificaciones de actualizacion de aplicaciones ', error)
                }

            })();

        } catch (error) {
            console.error('No se pudo notificar actualizacion de app ', error);
        }


    }
    _sendMessages(messageData) {
        firebaseConfig.sendMessage(messageData);
        // return axios.post(config.FIREBASE_MESSAGING_URL,
        //     messageData,
        //     {
        //         headers: {
        //             'Authorization': config.FIREBASE_AUTHORIZATION_HEADER,
        //             'Content-Type': 'application/json'
        //         }
        //     });
    }


}

module.exports = AppUpdatesCron;
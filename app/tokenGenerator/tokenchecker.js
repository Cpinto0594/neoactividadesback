let jwt = require('jsonwebtoken');
let config = require('./configuration.js');
let ResponseObject = require('../apiRoutes/ResponseObject');

let checkToken = async (req) => {
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
    if (token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
    }

    if (token) {
        let result = await jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                return new ResponseObject(req, false, {
                    message: 'Token is not valid',

                });
            } else {
                return new ResponseObject(req, true, {
                    message: 'Token is  valid',
                    decoded: decoded
                });
            }
        });
        return result;
    } else {
        return new ResponseObject(req, false, {
            message: 'Token is not Supplied'
        });
    }
};


let checkTokenMiddleWare = async (req, res, next) => {

    let response = await checkTokenV2(req);
    if (!response.success) {
        res.status(403).json(response)
        return;
    }
    //Si es user token refresh
    if (['NEW_USER_TOKEN'].indexOf(response.data.errorType) !== -1) {
        req.tokenRefresh = response.data;
    }
    next()
};


let checkTokenV2 = async (req) => {

    let token = req.headers['x-access-token'] ||
        req.headers['authorization']; // Express headers are auto converted to lowercase

    let refreshToken = req.headers['x-rftk'];
    //token =refreshToken ;

    if (token && token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
    }
    if (token && refreshToken) {

        let validRefreshToken = await verifyToken(refreshToken);
        let validUserToken = await verifyToken(token);

        //Validamos el RefreshToken
        //RefreshToken Invalido
        if (validRefreshToken.error) {
            // //Obtenemos el payload del token anterior
            // let payload = await verifyToken(token, { ignoreExpiration: true });
            // let newPayload = cleanPayload(payload.decoded);
            // console.log('Expired Token', payload.decoded, newPayload);


            // //Creamos nuevos token
            // let refreshToken = createToken(req, newPayload);
            // let userToken = createToken(req, newPayload);

            return (new ResponseObject(req, false, '', {
                message: 'Invalid RF Token',
                errorType: 'INV_RF_TOKEN'
            }));

        } else if (validUserToken.error) {
            //Refresh Token del usuario NoValido 
            //Obtenemos el payload del token anterior y generamos token nuevo
            let payload = await verifyToken(token, { ignoreExpiration: true });
            let newPayload = cleanPayload(payload.decoded);

            //Creamos el nuevo token para el usuario
            let userToken = createToken(req, newPayload, config.userTokenAlive);

            return (new ResponseObject(req, true, '', {
                message: 'New Token',
                errorType: 'NEW_USER_TOKEN',
                usrNewToken: userToken
            }));

        } else {
            //Si el token del usuario es válido
            return (new ResponseObject(req, true, '', {
                message: 'Token is valid',
                errorType: 'VALID_TOKEN',
                decoded: validUserToken.decoded
            }));
        }

    } else {
        //Token Was not Sent
        return (new ResponseObject(req, false, '', {
            message: 'Token is not Supplied',
            errorType: 'NO_TOKEN'
        }));
    }
};

//Limpia la data del token
let cleanPayload = (payload) => {
    let _payload = Object.assign({}, payload);
    delete _payload['iat'];
    delete _payload['exp'];
    return _payload;
}

//Verifica el token
let verifyToken = (token, options) => {

    return new Promise((succ, fail) => {
        try {
            jwt.verify(token, config.secret, options, (err, decoded) => {
                succ({
                    error: err ? err.message : undefined,
                    decoded: decoded
                })
            });
        } catch (e) {
            succ({
                error: e.message
            });
        }
    });

}

//Crea un token
let createToken = (req, payloadData, timeAlive) => {
    if (!payloadData)
        throw Error('No se puede generar token de acceso ');

    var origin = (req.headers && req.headers['x-forwarded-for']) || req.connection.remoteAddress;
    payloadData.origin = origin;

    return jwt.sign(payloadData, config.secret,
        {
            expiresIn: timeAlive// expires in x 
        });
}

module.exports = {
    checkToken: checkToken,
    chackTokenV2: checkTokenV2,
    checkTokenMiddleWare: checkTokenMiddleWare,
    createToken: createToken
}
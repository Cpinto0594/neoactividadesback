module.exports = {
    secret: 'activityControlTokenGenerator',
    userTokenAlive: '1h',
    refreshTokenAlive: '5d'
};
#!/bin/bash
AWS_LAMBDA_FUNCION_NAME="ActivityControlBackend"
NODE_ZIP_FOLDER="/home/cpinto/Documents/node_layer"
LIBRARIES_ZIP_FILE="node_modules_zip.zip"
NODE_MODULES_S3_BUCKET="activitycontrolnodejslambdafiles"
AWS_LINRARIES_LAYER_NAME="ActivityControlLayer"
AWS_LIBRARIES_ZIP_FILE="$NODE_ZIP_FOLDER/$LIBRARIES_ZIP_FILE"
MESSAGE="¿Que quieres realizar?\n" 
MESSAGE="$MESSAGE 1) Desplegar la lambda.\n"
MESSAGE="$MESSAGE 2) Desplegar le layer de librerias.\n"
MESSAGE="$MESSAGE 0) Salir"


deploy_libraries_layer(){
    echo "Building library packages"
    npm run buildLambdaPackage
    echo "Copying zip to aws S3 bucket"
    aws s3 cp $AWS_LIBRARIES_ZIP_FILE s3://$NODE_MODULES_S3_BUCKET
    echo "Adding file public access permission"
    aws s3api put-object-acl --bucket $NODE_MODULES_S3_BUCKET --key $LIBRARIES_ZIP_FILE --acl public-read
    echo "Creating layer version"
    layer_creation_output=`aws lambda publish-layer-version --layer-name $AWS_LINRARIES_LAYER_NAME --description "My Node layer" --license-info "MIT" --content S3Bucket=$NODE_MODULES_S3_BUCKET,S3Key=$LIBRARIES_ZIP_FILE --compatible-runtimes  nodejs12.x nodejs10.x`
    #aws lambda publish-layer-version --layer-name $AWS_LINRARIES_LAYER_NAME --description "My Node layer" --license-info "MIT" --zip-file fileb://$AWS_LIBRARIES_ZIP_FILE  --compatible-runtimes  nodejs12.x nodejs10.x
    local LAYER_VERSION=`echo $layer_creation_output | jq '.Version'`
    local LAYER_ARN=`echo $layer_creation_output | jq '.LayerVersionArn'`
    echo "New Layer Version: $LAYER_VERSION, New Layer Arn: $LAYER_ARN"
    should_update_function_with_new_layer $LAYER_ARN
}

should_update_function_with_new_layer(){
    echo "¿Quieres actualizar la funcion con el nuevo layer $1? [y/n]"
    read answer
    case $answer in
        "y")
         update_function_with_new_layer $1
        ;;
        "n" | *)
        echo 'Bye ' &&  exit ;;
    esac

}

update_function_with_new_layer( ){
    aws lambda update-function-configuration --function-name $AWS_LAMBDA_FUNCION_NAME --layers [$1]
}

deploy_lambda(){
    rm package.yml || true
    echo 'packacking'
    sam package --template-file deployment.yml --output-template-file package.yml  --s3-bucket activitycontrolnodejslambdafiles --s3-prefix deployments
    echo 'Deploying'
    sam deploy  --template-file package.yml --stack-name ActivityControlStack  --capabilities CAPABILITY_IAM
}


################Start ####################
echo $MESSAGE
read option
case $option in 
    1)
    deploy_lambda
    ;;
    2)
    deploy_libraries_layer
    ;;
    0 | *)
    echo "bye" && exit 
    ;;
esac

